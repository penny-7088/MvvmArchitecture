package com.penny.module.learn.di;

import com.penny.common.di.ApplicationInject;
import com.penny.common.di.component.AppComponent;
import com.penny.common.di.component.BaseApplicationComponent;
import com.penny.common.di.scope.ApplicationScope;
import com.penny.module.learn.di.module.LearnModule;

import dagger.Component;

/**
 * com.penny.module.main.di
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@ApplicationScope
@Component(dependencies = AppComponent.class, modules = LearnModule.class)
public interface LearnComponent extends ApplicationInject {


}
