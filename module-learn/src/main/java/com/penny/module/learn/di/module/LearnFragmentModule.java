package com.penny.module.learn.di.module;

import com.penny.common.di.component.BaseFragmentSubComponent;
import com.penny.module.learn.mvvm.view.LearnFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * com.penny.module.main.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@Module(subcomponents = BaseFragmentSubComponent.class)
public abstract class LearnFragmentModule {

    @ContributesAndroidInjector
    abstract LearnFragment contributeLearnFragment ();
}
