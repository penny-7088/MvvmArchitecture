package com.penny.module.learn.mvvm.view;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.orhanobut.logger.Logger;
import com.penny.common.base.ui.BaseFragment;
import com.penny.common.base.ui.BaseLazyFragment;
import com.penny.moduel.R;
import com.penny.moduel.databinding.LearnFragmentMainLayoutBinding;
import com.penny.module.learn.mvvm.viewmodel.LearnViewModel;

/**
 * com.penny.module.im.mvvm
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/12
 */
public class LearnFragment extends BaseFragment<LearnViewModel, LearnFragmentMainLayoutBinding> {

    public static LearnFragment newInstance(Object[] args) {
        return new LearnFragment();
    }

    @Override
    public int getLayoutId() {
        return R.layout.learn_fragment_main_layout;
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        Logger.d("binding:" + mBinding + "<-->mViewModel" + mViewModel);
    }
}
