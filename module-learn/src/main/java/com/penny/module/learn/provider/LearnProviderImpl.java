package com.penny.module.learn.provider;

import android.content.Context;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.penny.common.base.ui.BaseFragment;
import com.penny.common.router.provider.IChatProvider;
import com.penny.common.router.provider.ILearnProvider;
import com.penny.module.learn.mvvm.view.LearnFragment;

import static com.penny.common.router.provider.IChatProvider.CHAT_SERVICE;
import static com.penny.common.router.provider.ILearnProvider.LEARN_SERVICE;

/**
 * com.penny.module.im.provider
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/12
 */
@Route(path = LEARN_SERVICE)
public class LearnProviderImpl implements ILearnProvider {
    @Override
    public LearnFragment newInstance(Object... args) {
        return LearnFragment.newInstance(args);
    }

    @Override
    public void init(Context context) {

    }
}
