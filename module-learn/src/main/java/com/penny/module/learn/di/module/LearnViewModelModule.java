package com.penny.module.learn.di.module;

import androidx.lifecycle.ViewModel;

import com.penny.common.di.scope.ViewModelScope;
import com.penny.module.learn.mvvm.viewmodel.LearnViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * com.penny.module.main.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@Module
public abstract class LearnViewModelModule {

    @Binds
    @IntoMap
    @ViewModelScope(LearnViewModel.class)
    abstract ViewModel bindContainerViewModel(LearnViewModel containerViewModel);


}
