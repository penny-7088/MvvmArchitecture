package com.penny.module.learn.app;


import androidx.annotation.NonNull;

import com.orhanobut.logger.Logger;
import com.penny.common.app.BaseApplication;
import com.penny.common.app.IAppLifecycle;
import com.penny.module.learn.di.DaggerLearnComponent;


/**
 * com.penny.module.timeline.app
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public class LearnLifecycle implements IAppLifecycle {


    @Override
    public void attachBaseContext(@NonNull BaseApplication base) {

    }

    @Override
    public void onCreate(@NonNull BaseApplication application) {
        Logger.d("LearnLifecycle 模块 初始化");

    }


    @Override
    public void onTerminate(@NonNull BaseApplication application) {

    }

    @Override
    public void onLowMemory(@NonNull BaseApplication application) {

    }

    @Override
    public void onTrimMemory(@NonNull BaseApplication application, int level) {

    }

    @Override
    public void injectChildModule(@NonNull BaseApplication baseApplication) {
        DaggerLearnComponent.builder().appComponent(baseApplication.getAppComponent())
                .build().inject(baseApplication);
    }

}
