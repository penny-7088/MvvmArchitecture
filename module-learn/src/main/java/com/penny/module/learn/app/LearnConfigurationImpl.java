package com.penny.module.learn.app;

import android.content.Context;

import com.penny.common.app.IAppLifecycle;
import com.penny.common.config.ConfigModule;
import com.penny.common.di.module.GlobalConfigModule;

import java.util.List;

/**
 * com.penny.module.timeline.app
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public class LearnConfigurationImpl implements ConfigModule {


    @Override
    public void applyOptions(Context context, GlobalConfigModule.Builder builder) {

    }

    @Override
    public void injectAppLifecycle(Context context, List<IAppLifecycle> lifecycles) {
        lifecycles.add(new LearnLifecycle());
    }
}
