package com.penny.module.chat.di.componentt;

import com.penny.common.di.ApplicationInject;
import com.penny.common.di.component.AppComponent;
import com.penny.common.di.component.BaseApplicationComponent;
import com.penny.common.di.scope.ApplicationScope;
import com.penny.module.chat.di.ChatFragmentModule;
import com.penny.module.chat.di.ChatModule;
import com.penny.module.chat.di.ChatViewModelModule;

import dagger.Component;

/**
 * com.penny.module.timeline.di
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@ApplicationScope
@Component(dependencies = AppComponent.class, modules = ChatModule.class)
public interface ChatComponent extends ApplicationInject {



}
