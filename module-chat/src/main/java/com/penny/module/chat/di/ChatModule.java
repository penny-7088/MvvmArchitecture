package com.penny.module.chat.di;

import com.penny.common.di.module.ViewModelFactoryModule;
import com.penny.module.chat.mvvm.model.Test;

import dagger.Module;
import dagger.Provides;

/**
 * com.penny.module.timeline.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@Module(includes = {ViewModelFactoryModule.class, ChatFragmentModule.class, ChatViewModelModule.class})
public class ChatModule {

    @Provides
    static Test provideTest() {
        Test test = new Test();
        test.setName("penny");
        return test;
    }

}
