package com.penny.module.chat.mvvm.model;

/**
 * com.penny.module.chat.mvvm.model
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/7/1
 */
public class Test {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
