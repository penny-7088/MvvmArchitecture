package com.penny.module.chat.provider;

import android.content.Context;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.penny.common.router.provider.IChatProvider;
import com.penny.module.chat.mvvm.view.ChatFragment;

import static com.penny.common.router.provider.IChatProvider.CHAT_SERVICE;

/**
 * com.penny.module.timeline.provider
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/11
 */
@Route(path = CHAT_SERVICE)
public class ChatProviderImpl implements IChatProvider {


    private Context mContext;

    private ChatFragment fragment;

    /**
     * 也可以通过初始化的时候 通过bundle 传值 也可以通过{@link com.penny.common.router.Router} 来传值 方法很多种
     *
     * @param args
     * @return
     */
    @Override
    public ChatFragment newInstance(Object... args) {
        fragment = ChatFragment.newInstance(args);
        return ChatFragment.newInstance(args);
    }


    @Override
    public void init(Context context) {
        this.mContext = context;
    }

    @Override
    public void setChatData(String data) {
        fragment.setData(data);
    }
}
