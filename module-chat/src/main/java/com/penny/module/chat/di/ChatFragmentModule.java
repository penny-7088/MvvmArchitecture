package com.penny.module.chat.di;

import com.penny.common.di.component.BaseFragmentSubComponent;
import com.penny.common.di.scope.FragmentScope;
import com.penny.module.chat.mvvm.view.ChatFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * com.penny.module.timeline.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@Module(subcomponents = BaseFragmentSubComponent.class)
public abstract class ChatFragmentModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract ChatFragment contributeChatFragment();

}
