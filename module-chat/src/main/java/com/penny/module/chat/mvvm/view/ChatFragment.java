package com.penny.module.chat.mvvm.view;


import android.os.Bundle;

import androidx.annotation.Nullable;

import com.orhanobut.logger.Logger;
import com.penny.common.base.ui.BaseFragment;
import com.penny.module.chat.R;
import com.penny.module.chat.databinding.ChatFragmentMainLayoutBinding;
import com.penny.module.chat.mvvm.model.Test;
import com.penny.module.chat.mvvm.viewmodel.ChatViewModel;

import java.util.Objects;

import javax.inject.Inject;

/**
 * com.penny.module.timeline.app
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public class ChatFragment extends BaseFragment<ChatViewModel, ChatFragmentMainLayoutBinding> {

    @Inject
    Test test;

    public static ChatFragment newInstance(Object[] args) {
        return new ChatFragment();
    }

    @Override
    public int getLayoutId() {
        return R.layout.chat_fragment_main_layout;
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        if (arguments != null) {
            String key = arguments.getString("key");
            Logger.d(key);
        }

        Logger.d(test.getName());
        mViewModel.storage(test.getName());

    }

    public void setData(String data) {
        Logger.d(data);
    }
}
