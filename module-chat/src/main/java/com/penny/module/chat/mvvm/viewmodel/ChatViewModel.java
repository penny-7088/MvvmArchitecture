package com.penny.module.chat.mvvm.viewmodel;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.orhanobut.logger.Logger;
import com.penny.common.base.model.BaseModel;
import com.penny.common.base.viewmodel.DataViewModel;
import com.penny.common.storage.local.KVCacheModel;
import com.penny.common.storage.memory.Cache;

import javax.inject.Inject;

/**
 * com.penny.module.timeline.mvvm.viewmodel
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public class ChatViewModel extends DataViewModel {

    @Inject
    public ChatViewModel(@NonNull Application application, BaseModel model) {
        super(application, model);
    }


    @Override
    public void onCreate() {
        super.onCreate();

        Logger.d("ChatViewModel init" + getKVCacheHelper());
//        Logger.d("ChatViewModel init" + getRetrofitService());

    }

    public void storage(String name) {
        if (!TextUtils.isEmpty(name)) {
            KVCacheModel key = getKVCacheHelper().newModel("key");
            key.encode("penny", name);
            Logger.d(key.decodeString("penny", "no value"));

            Cache<String, Object> cache = getCache();
            cache.put("name", name);
            Logger.d(cache.get("name"));


        }
    }
}
