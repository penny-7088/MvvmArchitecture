package com.penny.module.chat.di;

import androidx.lifecycle.ViewModel;

import com.penny.common.di.scope.ViewModelScope;
import com.penny.module.chat.mvvm.viewmodel.ChatViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * com.penny.module.timeline.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@Module
public abstract class ChatViewModelModule {

    @Binds
    @IntoMap
    @ViewModelScope(ChatViewModel.class)
    abstract ViewModel bindContainerViewModel(ChatViewModel chatViewModel);


}
