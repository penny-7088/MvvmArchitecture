package com.penny.module.sign.mvvm.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.penny.common.base.ui.BaseActivity;
import com.penny.module.R;
import com.penny.module.databinding.SignActivityBinding;
import com.penny.module.sign.mvvm.viewmodel.SignViewModel;

/**
 * com.penny.module.sign
 *
 * @author :
 * @describe :
 * @date : 2020/06/28
 */
public class SignActivity extends BaseActivity<SignViewModel, SignActivityBinding> {


    @Override
    public int getLayoutId() {
        return R.layout.sign_activity;
    }


    @Override
    public void initData(@Nullable Bundle savedInstanceState) {


    }
}
