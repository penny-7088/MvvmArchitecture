package com.penny.module.sign.mvvm.model;

import com.penny.common.base.model.BaseModel;
import com.penny.common.storage.IDataRepository;

import javax.inject.Inject;

/**
 * com.penny.module.sign
 *
 * @author :
 * @describe :
 * @date : 2020/06/29
 */
public class DemoModel extends BaseModel {

    @Inject
    public DemoModel(IDataRepository dataRepository) {
        super(dataRepository);
    }


}
