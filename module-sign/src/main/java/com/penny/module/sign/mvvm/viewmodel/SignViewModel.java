package com.penny.module.sign.mvvm.viewmodel;

import android.app.Application;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;

import com.penny.common.base.viewmodel.BaseViewModel;
import com.penny.module.sign.mvvm.model.SignModel;

import javax.inject.Inject;

/**
 * com.penny.module.sign
 *
 * @author :
 * @describe :
 * @date : 2020/06/28
 */
public class SignViewModel extends BaseViewModel<SignModel> {

    @Inject
    public SignViewModel(@NonNull Application application, SignModel model) {
        super(application, model);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

}
