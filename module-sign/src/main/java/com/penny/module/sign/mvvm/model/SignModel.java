package com.penny.module.sign.mvvm.model;

import androidx.appcompat.app.AppCompatActivity;

import com.penny.common.base.model.BaseModel;
import com.penny.common.storage.IDataRepository;

import javax.inject.Inject;


/**
 * com.penny.module.sign
 *
 * @author :
 * @describe :
 * @date : 2020/06/28
 */
public class SignModel extends BaseModel {

    @Inject
    public SignModel(IDataRepository dataRepository) {
        super(dataRepository);
    }


}
