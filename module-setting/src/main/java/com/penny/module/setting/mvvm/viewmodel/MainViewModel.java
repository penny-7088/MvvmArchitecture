package com.penny.module.setting.mvvm.viewmodel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;

import javax.inject.Inject;

import android.app.Application;

import com.penny.common.base.viewmodel.BaseViewModel;
import com.penny.module.setting.mvvm.model.MainModel;

/**
 * com.penny.module.setting
 *
 * @author :
 * @describe :
 * @date : 2020/07/02
 */
public class MainViewModel extends BaseViewModel<MainModel> {

    @Inject
    public MainViewModel(@NonNull Application application, MainModel model) {
        super(application, model);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

}
