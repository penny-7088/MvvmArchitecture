package com.penny.module.setting.mvvm.view.fragment;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.penny.common.base.ui.BaseFragment;
import com.penny.module.R;
import com.penny.module.databinding.SettingMainFragmentBinding;
import com.penny.module.setting.mvvm.viewmodel.MainViewModel;


/**
 * com.penny.module.setting
 *
 * @author :
 * @describe :
 * @date : 2020/07/02
 */
public class MainFragment extends BaseFragment<MainViewModel, SettingMainFragmentBinding> {


    public static MainFragment newInstance(Object[] args) {
        return new MainFragment();
    }

    @Override
    public int getLayoutId() {
        return R.layout.setting_main_fragment;
    }


    @Override
    public void initData(@Nullable Bundle savedInstanceState) {


    }
}
