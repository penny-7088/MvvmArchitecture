package com.penny.module.setting.mvvm.view.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.penny.common.base.ui.BaseActivity;
import com.penny.module.R;
import com.penny.module.databinding.SettingDevActivityBinding;
import com.penny.module.setting.mvvm.viewmodel.DevViewModel;


/**
 * com.penny.module.setting
 *
 * @author :
 * @describe :
 * @date : 2020/07/02
 */
public class DevActivity extends BaseActivity<DevViewModel, SettingDevActivityBinding> {


    @Override
    public int getLayoutId() {
        return R.layout.setting_dev_activity;
    }


    @Override
    public void initData(@Nullable Bundle savedInstanceState) {


    }
}
