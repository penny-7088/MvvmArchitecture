package com.penny.module.setting.di.module;

import androidx.appcompat.app.AppCompatActivity;

import dagger.Module;

/**
 * com.penny.module.setting
 *
 * @author :
 * @describe :
 * @date : 2020/07/02
 */
@Module(subcomponents = BaseFragmentSubComponent.class)
public abstract class MainFragmentModule {
}
