package com.penny.module.setting.mvvm.viewmodel;

import android.app.Application;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;

import javax.inject.Inject;

import com.penny.common.base.viewmodel.BaseViewModel;
import com.penny.module.setting.mvvm.model.DevModel;

/**
 * com.penny.module.setting
 *
 * @author :
 * @describe :
 * @date : 2020/07/02
 */
public class DevViewModel extends BaseViewModel<DevModel> {

    @Inject
    public DevViewModel(@NonNull Application application, DevModel model) {
        super(application, model);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

}
