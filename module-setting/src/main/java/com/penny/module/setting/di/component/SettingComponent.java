package com.penny.module.setting.di.component;

import androidx.appcompat.app.AppCompatActivity;


/**
 * com.penny.module.setting
 *
 * @author :
 * @describe :
 * @date : 2020/07/02
 */
@Component(dependencies = AppComponent.class, modules = SettingModule.class)
public interface SettingComponent extends ApplicationInject {


}
