package com.penny.module.setting.mvvm.model;

import androidx.appcompat.app.AppCompatActivity;

import com.penny.common.base.model.BaseModel;
import com.penny.common.storage.IDataRepository;

import javax.inject.Inject;

/**
 * com.penny.module.setting
 *
 * @author :
 * @describe :
 * @date : 2020/07/02
 */
public class MainModel extends BaseModel {

    @Inject
    public MainModel(IDataRepository dataRepository) {
        super(dataRepository);
    }


}
