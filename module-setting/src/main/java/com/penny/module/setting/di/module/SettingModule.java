package com.penny.module.setting.di.module;

import androidx.appcompat.app.AppCompatActivity;


/**
 * com.penny.module.setting
 *
 * @author :
 * @describe :
 * @date : 2020/07/02
 */
@Module(includes = {ViewModelFactoryModule.class,
        MainActivityModule.class,
        MainFragmentModule.class,
        MainViewModelModule.class})
public class SettingModule {

}
