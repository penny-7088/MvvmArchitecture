package com.penny.common.util;

import com.penny.common.consts.RegexConstants;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * com.penny.commonbase.util
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/1
 */
public class RegexUtils {
    /**
     * 判断是否匹配正则
     *
     * @param regex 正则表达式
     * @param input 要匹配的字符串
     * @return {@code true}: 匹配<br>{@code false}: 不匹配
     */
    private static boolean isMatch(final String regex, final CharSequence input) {
        return input != null && input.length() > 0 && Pattern.matches(regex, input);
    }

    /**
     * 验证手机号（简单）
     *
     * @param input 待验证文本
     * @return {@code true}: 匹配<br>{@code false}: 不匹配
     */
    public static boolean isMobileSimple(final CharSequence input) {
        return isMatch(RegexConstants.REGEX_MOBILE_SIMPLE, input);
    }


    /**
     * 验证手机号（精确）
     *
     * @param input 待验证文本
     * @return {@code true}: 匹配<br>{@code false}: 不匹配
     */
    public static boolean isMobileExact(final CharSequence input) {
        return isMatch(RegexConstants.REGEX_MOBILE_EXACT, input);
    }


    /**
     * 验证身份证号码 15 位
     *
     * @param input 待验证文本
     * @return {@code true}: 匹配<br>{@code false}: 不匹配
     */
    public static boolean isIDCard15(final CharSequence input) {
        return isMatch(RegexConstants.REGEX_ID_CARD15, input);
    }

    /**
     * 验证身份证号码 18 位
     *
     * @param input 待验证文本
     * @return {@code true}: 匹配<br>{@code false}: 不匹配
     */
    public static boolean isIDCard18(final CharSequence input) {
        return isMatch(RegexConstants.REGEX_ID_CARD18, input);
    }

    /**
     * 判断是否是合法验证码（4位数字）
     *
     * @param input 待验证文本
     * @return {@code true}: 匹配<br>{@code false}: 不匹配
     */
    public static boolean isCaptcha(final CharSequence input) {
        return isMatch(RegexConstants.REGEX_CAPTCHA, input);
    }


    /**
     * 给定的url是否为图片格式
     */
    public static boolean isPicSuffix(String url) {
        Pattern pattern = Pattern.compile(RegexConstants.REGEX_IMG);
        Matcher matcher = pattern.matcher(url);
        return matcher.matches();
    }

    /**
     * 金额格式转换
     *
     * @param money
     * @return
     */
    public static String getIntgerDecimal(double money) {
        DecimalFormat formater = new DecimalFormat("#,##0.00");
        formater.setRoundingMode(RoundingMode.HALF_UP);
        String string = formater.format(money);
        if (".00".equals(string)) {
            string = "0.00";
        }
        return string;

    }

}
