package com.penny.common.util;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.ArrayRes;
import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;

import com.penny.common.app.BaseApplication;


/**
 * @Author 黄林
 * @Time 2018/6/5 下午4:57
 * @Description Resources获取工具类
 */
public class ResourcesUtils {

    public static String getString(@StringRes int stringId) {
        return AccessToResources.getString(stringId);
    }

//    public static String getString(@StringRes int stringId, Object... pram) {
//        return AccessToResources.getString(stringId, pram);
//    }
//
    public static int getColor(@ColorRes int colorId) {
        return ContextCompat.getColor(BaseApplication.getInstance().getAppComponent().getApplication(), colorId);
    }

    public static Drawable getDrawable(@DrawableRes int drawableId) {
        return AppCompatResources.getDrawable(BaseApplication.getInstance().getAppComponent().getApplication(), drawableId);
    }

    public static float getDimension(@DimenRes int dimenId) {
        return BaseApplication.getInstance().getAppComponent().getApplication().getResources().getDimension(dimenId);
    }

    public static int getDimensionPixelSize(@DimenRes int dimenId) {
        return BaseApplication.getInstance().getAppComponent().getApplication().getResources().getDimensionPixelSize(dimenId);
    }

//    public static String[] getStringArray(@ArrayRes int arrayId) {
//        return AccessToResources.getStringArray(arrayId);
//    }
}
