package com.penny.common.util;

import android.content.Context;
import android.content.res.Resources;

import com.penny.common.app.BaseApplication;

public class AccessToResources {

    public static int getStringRes(String name, Context mContext) {
        final Resources res = mContext.getResources();
        return res.getIdentifier(name, "string", "PackagetUtils.PACKAGENAME");
    }

    public static String getString(String name, Context mContext) {
        final Resources res = mContext.getResources();
        return res.getString(res.getIdentifier(name, "string", "PackagetUtils.PACKAGENAME"));
    }

    public static String[] getArrayString(String name, Context mContext) {
        final Resources res = mContext.getResources();
        return res.getStringArray(res.getIdentifier(name, "array", "PackagetUtils.PACKAGENAME"));
    }

    //    public static String getString(String name, Object... pram) {
//        final Resources res = getAccessResource();
//        try {
//            return res.getString(res.getIdentifier(name, "string", "PackagetUtils.PACKAGENAME"), pram);
//        } catch (Exception e) {
//            Logger.e("AccessToResources", "getString", e);
//            return "";
//        }
//    }
//
//    public static String getString(String name) {
//        final Resources res = getAccessResource();
//        try {
//            return res.getString(res.getIdentifier(name, "string", PackagetUtils.PACKAGENAME));
//        } catch (Exception e) {
//            return "";
//        }
//    }
//
    public static String getString(int resID) {
        final Resources res = CommonUtils.getResources(BaseApplication.getInstance().getAppComponent().getApplication());
        try {
            return res.getString(resID);
        } catch (Exception e) {
            return "";
        }
    }
//
//    public static String getString(int resID, Object... pram) {
//        final Resources res = getAccessResource();
//        try {
//            return res.getString(resID, pram);
//        } catch (Exception e) {
//            return "";
//        }
//
//    }
//
//    public static String[] getStringArray(String name) {
//        final Resources res = getAccessResource();
//        try {
//            return res.getStringArray(res.getIdentifier(name, "array", PackagetUtils.PACKAGENAME));
//        } catch (Exception e) {
//            return null;
//        }
//
//    }
//
//    public static String[] getStringArray(int resID) {
//        final Resources res = getAccessResource();
//        try {
//            return res.getStringArray(resID);
//        } catch (Exception e) {
//            return null;
//        }
//
//    }
//
//    public static int getDrawableRes(String name, Context mContext) {
//        final Resources res = mContext.getResources();
//        return res.getIdentifier(name, "drawable", PackagetUtils.PACKAGENAME);
//    }
//
//    public static int getDrawableRes(String name) {
//        final Resources res = getAccessResource();
//        return res.getIdentifier(name, "drawable", PackagetUtils.PACKAGENAME);
//    }
//
//    public static Drawable getDrawable(String name) {
//        try {
//            final Resources res = getAccessResource();
//            final int resID = res.getIdentifier(name, "drawable", PackagetUtils.PACKAGENAME);
//            return res.getDrawable(resID);
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    public static int getColor(String name) {
//        final Resources res = getAccessResource();
//        final int resID = res.getIdentifier(name, "color", PackagetUtils.PACKAGENAME);
//        return res.getColor(resID);
//    }
//
//

}
