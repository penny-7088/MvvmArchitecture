package com.penny.common.app;

import android.app.Application;

import androidx.annotation.NonNull;

/**
 * MvvmArchitecture
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/4
 **/
public interface IAppLifecycle {

    /**
     * 在{@link Application# attachBaseContext(Context)} 中执行
     *
     * @param base
     */
    void attachBaseContext(@NonNull BaseApplication base);

    /**
     * 在{@link Application#onCreate()} 中执行
     */
    void onCreate(@NonNull BaseApplication application);


    /**
     * 在{@link Application#onTerminate()} 中执行
     */
    void onTerminate(@NonNull BaseApplication application);

    /**
     * 在{@link Application#onLowMemory()} 中执行
     */
    void onLowMemory(@NonNull BaseApplication application);

    /**
     * 在{@link Application#onTrimMemory(int)} 中执行
     */
    void onTrimMemory(@NonNull BaseApplication application, int level);

    /**
     * 在 {@link BaseApplication # 的子类会执行，只有在单独运行每个模块时执行}
     */
    void injectChildModule(@NonNull BaseApplication baseApplication);
}
