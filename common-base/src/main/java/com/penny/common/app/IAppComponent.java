package com.penny.common.app;

import androidx.annotation.NonNull;

import com.penny.common.di.component.AppComponent;

/**
 * Package:     MvvmArchitecture
 * User:        Penny
 * Date:        2020/6/1
 * Description:  框架要求框架中的每个 {@link android.app.Application} 都需要实现此类, 以满足规范
 **/
public interface IAppComponent {

    @NonNull
    AppComponent getAppComponent();
}
