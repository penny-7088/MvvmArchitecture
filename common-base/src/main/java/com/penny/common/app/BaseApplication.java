package com.penny.common.app;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;

import com.penny.common.BuildConfig;
import com.penny.common.di.component.AppComponent;
import com.penny.common.di.component.DaggerBaseApplicationComponent;
import com.penny.common.util.Preconditions;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import timber.log.Timber;

/**
 * Package:     MvvmArchitecture
 * User:        Penny
 * Date:        2020/6/1
 * Description:
 **/
public class BaseApplication extends Application implements IAppComponent, HasAndroidInjector {

    protected AppDelegateImpl mAppDelegateImpl;

    /**
     * Dagger.Android 注入 dagger2 直接再application  里面注入 减免了以前的模板代码
     * 不需要每个act,frag 去做注入
     */
    @Inject
    DispatchingAndroidInjector<Object> mAndroidInjector;


    public static BaseApplication instance;

    public static BaseApplication getInstance() {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
        if (mAppDelegateImpl == null)
            this.mAppDelegateImpl = new AppDelegateImpl(base);
        this.mAppDelegateImpl.attachBaseContext(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (mAppDelegateImpl != null)
            this.mAppDelegateImpl.onCreate(this);

    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mAppDelegateImpl != null)
            this.mAppDelegateImpl.onLowMemory(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        if (mAppDelegateImpl != null)
            this.mAppDelegateImpl.onTerminate(this);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (mAppDelegateImpl != null)
            this.mAppDelegateImpl.onTrimMemory(this, level);
    }


    @NonNull
    @Override
    public AppComponent getAppComponent() {
        Preconditions.checkNotNull(mAppDelegateImpl, "%s cannot be null", AppDelegateImpl.class.getName());
        Preconditions.checkState(mAppDelegateImpl instanceof IAppComponent, "%s must be implements %s", AppDelegateImpl.class.getName(), IAppComponent.class.getName());
        return ((IAppComponent) mAppDelegateImpl).getAppComponent();
    }


    @Override
    public AndroidInjector<Object> androidInjector() {
        return mAndroidInjector;
    }
}
