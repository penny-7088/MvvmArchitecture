package com.penny.common.app;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;

import com.penny.common.config.ConfigModule;
import com.penny.common.config.ManifestParser;
import com.penny.common.di.component.AppComponent;
import com.penny.common.di.component.DaggerAppComponent;
import com.penny.common.di.module.GlobalConfigModule;
import com.penny.common.util.Preconditions;

import java.util.ArrayList;
import java.util.List;

/**
 * com.penny.commonbase
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe : app 委托代理类，方便于其他模块需要初始自己的模块所需要的东西
 * @date : 2020/5/9
 */
public class AppDelegateImpl implements IAppLifecycle, IAppComponent {

    private final List<ConfigModule> mModules;

    private List<IAppLifecycle> mAppLifecycles = new ArrayList<>();

    private AppComponent mAppComponent;

    public List<IAppLifecycle> getAppLifecycle() {
        return mAppLifecycles;
    }

    public void setAppLifecycle(List<IAppLifecycle> mAppLifecycles) {
        this.mAppLifecycles = mAppLifecycles;
    }

    private Application mApplication;

    public AppDelegateImpl(Context context) {
        this.mModules = new ManifestParser(context).parse();
        for (ConfigModule module : mModules) {
            //将框架外部, 开发者实现的 Application 的生命周期回调 (AppLifecycles) 存入 mAppLifecycles 集合 (此时还未注册回调)
            module.injectAppLifecycle(context, mAppLifecycles);
        }
    }

    @Override
    public void attachBaseContext(@NonNull BaseApplication base) {
        for (IAppLifecycle lifecycle : mAppLifecycles) {
            lifecycle.attachBaseContext(base);
        }

    }

    /**
     * 实现了 {@link IAppLifecycle Dagger 自动全部注入，如果模块中没有需要初始化的sdk之类 可以不做任何实现，但是必须在模块中注册 <meta>
     *     注入一些配置信息
     * {@link com.penny.common.config.GlobalConfigurationImpl}
     *
     * @param application
     */
    @Override
    public void onCreate(@NonNull BaseApplication application) {
        this.mApplication = application;
        this.mAppComponent = DaggerAppComponent.builder()
                .application(application)
                .globalConfigModule(getGlobalConfigModule(application, mModules))
                .build();
        mAppComponent.inject(this);

        /**
         * 不同模块的初始操作
         */
        for (IAppLifecycle lifecycle : mAppLifecycles) {
            lifecycle.onCreate(application);
        }
    }


    /**
     * 获取全局配置 可自定义配置参数 {@link GlobalConfigModule}
     *
     * @param context
     * @param modules
     * @return
     */
    private GlobalConfigModule getGlobalConfigModule(Context context, List<ConfigModule> modules) {

        GlobalConfigModule.Builder builder = GlobalConfigModule
                .builder();
        //遍历 ConfigModule 集合, 给全局配置 GlobalConfigModule 添加参数
        for (ConfigModule module : modules) {
            module.applyOptions(context, builder);
        }

        return builder.build();
    }

    @Override
    public void onTerminate(@NonNull BaseApplication application) {
        if (mAppLifecycles != null && mAppLifecycles.size() > 0) {
            for (IAppLifecycle lifecycle : mAppLifecycles) {
                lifecycle.onTerminate(application);
            }
        }
        this.mAppComponent = null;
        this.mAppLifecycles = null;
        this.mApplication = null;
    }

    @Override
    public void onLowMemory(@NonNull BaseApplication application) {
        if (mAppLifecycles != null && mAppLifecycles.size() > 0) {
            for (IAppLifecycle lifecycle : mAppLifecycles) {
                lifecycle.onLowMemory(application);
            }
        }
    }

    @Override
    public void onTrimMemory(@NonNull BaseApplication application, int level) {
        if (mAppLifecycles != null && mAppLifecycles.size() > 0) {
            for (IAppLifecycle lifecycle : mAppLifecycles) {
                lifecycle.onTrimMemory(application, level);
            }
        }
    }

    @Override
    public void injectChildModule(@NonNull BaseApplication baseApplication) {
        if (mAppLifecycles != null && mAppLifecycles.size() > 0) {
            for (IAppLifecycle lifecycle : mAppLifecycles) {
                lifecycle.injectChildModule(baseApplication);
            }
        }
    }


    @NonNull
    @Override
    public AppComponent getAppComponent() {
        Preconditions.checkNotNull(mAppComponent, "%s cannot be null", AppComponent.class.getName());
        return mAppComponent;
    }

}
