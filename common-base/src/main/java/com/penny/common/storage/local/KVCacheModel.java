package com.penny.common.storage.local;

import android.content.SharedPreferences;
import android.text.TextUtils;

import com.orhanobut.logger.Logger;
import com.tencent.mmkv.MMKV;

/**
 * Created on 2019/12/17.
 * 用于key-value缓存的实例
 *
 * @author wind
 */
public class KVCacheModel {

    private final static String TAG = "KVCacheModel";

    private MMKV kv;

    public KVCacheModel(String modelKey) {
        try {
            kv = MMKV.mmkvWithID(modelKey);
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
    }

    public KVCacheModel(String modelKey, String cryptKey) {
        try {
            kv = MMKV.mmkvWithID(modelKey, MMKV.SINGLE_PROCESS_MODE, cryptKey);
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
    }

    public KVCacheModel(String modelKey, int mode, String cryptKey) {
        try {
            kv = MMKV.mmkvWithID(modelKey, mode, cryptKey);
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
    }

    public boolean encode(String key, int value) {
        if (kv == null) {
            return false;
        }

        return kv.encode(key, value);
    }

    public boolean encode(String key, long value) {
        if (kv == null) {
            return false;
        }
        return kv.encode(key, value);
    }

    public boolean encode(String key, String value) {
        if (kv == null) {
            return false;
        }

        return kv.encode(key, value);
    }

    public boolean encode(String key, boolean value) {
        if (kv == null) {
            return false;
        }

        return kv.encode(key, value);
    }

    public boolean encode(String key, double value) {
        if (kv == null) {
            return false;
        }
        return kv.encode(key, value);
    }

    public boolean encode(String key, float value) {
        if (kv == null) {
            return false;
        }

        return kv.encode(key, value);
    }

    public int decodeInt(String key, int defaultValue) {
        if (kv == null) {
            return defaultValue;
        }

        return kv.decodeInt(key, defaultValue);
    }

    public String decodeString(String key, String defaultValue) {
        if (kv == null) {
            return null;
        }

        return kv.decodeString(key, defaultValue);
    }

    public long decodeLong(String key, long defaultValue) {
        if (kv == null) {
            return defaultValue;
        }

        return kv.decodeLong(key, defaultValue);
    }

    public boolean decodeBoolean(String key, boolean defaultValue) {
        if (kv == null) {
            return defaultValue;
        }

        return kv.decodeBool(key, defaultValue);
    }

    public double decodeDouble(String key, double defaultValue) {
        if (kv == null) {
            return defaultValue;
        }

        return kv.decodeDouble(key, defaultValue);
    }

    public float decodeFloat(String key, float defaultValue) {
        if (kv == null) {
            return defaultValue;
        }

        return kv.decodeFloat(key, defaultValue);
    }

    public void removeKey(String key) {
        if (kv != null) {
            kv.removeValueForKey(key);
        }
    }

    public boolean containKey(String key) {
        if (kv == null) {
            return false;
        }
        if (TextUtils.isEmpty(key)) {
            return false;
        }

        return kv.containsKey(key);

    }

    public void clearAll() {
        if (kv == null) {
            return;
        }

        kv.clearAll();
    }

    public boolean isEmpty() {
        if (kv == null) {
            return true;
        }

        String[] keys = kv.allKeys();
        return keys == null || keys.length == 0;
    }

    public void importFromShared(SharedPreferences preferences) {
        if (kv != null && preferences != null) {
            Logger.e(TAG, "importFromShared");
            kv.importFromSharedPreferences(preferences);
        }
    }

}
