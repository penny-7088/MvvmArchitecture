package com.penny.common.storage.local;

public class SwitchKVCacheModel extends KVCacheModel {

    public final static int SWITCH_ON = 1;
    public final static int SWITCH_OFF = 0;

    /**
     * 控制隐藏搜索信息流的开关
     */
    private static final String KEY_SHOW_SEARCH_MOMENT = "key_show_search_moment";
    /**
     * 发帖开关
     */
    private static final String KEY_SHOW_POST_MOMENT = "key_show_post_moment";

    /**
     * 信息流通知开关
     */
    private static final String KEY_SHOW_NOTIFICATION_MOMENT = "key_show_notification_moment";

    /**
     * 昵称和电邮搜索开关
     */
    private static final String KEY_SHOW_NICK_EMAIL_SEARCH = "key_nick_email_search";

    /**
     * 台湾省的国旗开关
     */
    private static final String KEY_FLAG_TW = "key_flag_tw";

    /**
     * 台湾省的名称开关
     */
    private static final String KEY_NAME_TW = "key_name_tw";

    /**
     * 控制头像特殊时期不可修改
     */
    private static final String KEY_ENABLE_MODIFY_USER_HEADER = "key_enable_modify_user_header";

    /**
     * /ID/特殊时期不可修改
     */
    private static final String KEY_ENABLE_MODIFY_USER_ID = "key_enable_modify_user_id";

    /**
     * 昵称特殊时期不可修改
     */
    private static final String KEY_ENABLE_MODIFY_USER_NICK = "key_enable_modify_user_nick";

    /**
     * 自我介绍特殊时期不可修改
     */
    private static final String KEY_ENABLE_MODIFY_USER_INTRODUCE = "key_enable_modify_user_introduce";

    public SwitchKVCacheModel(String modelKey) {
        super(modelKey);
    }

    /**
     * 保存搜索信息流的开关
     *
     * @param state 只能是SWITCH_ON或者SWITCH_OFF两种状态
     */
    public void saveShowSearchMomentSwitch(int state) {
        encode(KEY_SHOW_SEARCH_MOMENT, state);
    }

    /**
     * 是否已经显示信息流搜索，默认不隐藏
     *
     * @return true=显示
     */
    public boolean doesShowSearchMoment() {
        return decodeInt(KEY_SHOW_SEARCH_MOMENT, SWITCH_ON) == SWITCH_ON;
    }

    /**
     * 保存发帖入口开关
     *
     * @param state 只能是SWITCH_ON或者SWITCH_OFF两种状态
     */
    public void saveShowPostMoment(int state) {
        encode(KEY_SHOW_POST_MOMENT, state);
    }

    /**
     * 是否已经打开了发帖入口，默认打开
     *
     * @return true=打开
     */
    public boolean doesShowPostMoment() {
        return decodeInt(KEY_SHOW_POST_MOMENT, SWITCH_ON) == SWITCH_ON;
    }

    /**
     * 保存信息流通知入口开关，默认打开
     *
     * @param state 只能是SWITCH_ON或者SWITCH_OFF两种状态
     */
    public void saveShowNotificationMoment(int state) {
        encode(KEY_SHOW_NOTIFICATION_MOMENT, state);
    }

    /**
     * 是否已经打开了信息流通知入口，默认打开
     *
     * @return true=打开
     */
    public boolean doesShowNotificationMoment() {
        return decodeInt(KEY_SHOW_NOTIFICATION_MOMENT, SWITCH_ON) == SWITCH_ON;
    }

    /**
     * 保存昵称/电邮搜索入口开关
     *
     * @param state 只能是SWITCH_ON或者SWITCH_OFF两种状态
     */
    public void saveShowNickEmailSearch(int state) {
        encode(KEY_SHOW_NICK_EMAIL_SEARCH, state);
    }

    /**
     * 是否已经打开了昵称/电邮搜索入口，默认打开
     *
     * @return true=打开
     */
    public boolean doesShowNickEmailSearch() {
        return decodeInt(KEY_SHOW_NICK_EMAIL_SEARCH, SWITCH_ON) == SWITCH_ON;
    }

    /**
     * 保存台湾省国旗和名称的后缀开关
     *
     * @param state 只能是SWITCH_ON或者SWITCH_OFF两种状态
     */
    public void saveTWFlag(int state) {
        encode(KEY_FLAG_TW, state);
    }

    /**
     * 是否打开台湾省的旗帜
     *
     * @return true=打开
     */
    public boolean doesTWFlag() {
        return decodeInt(KEY_FLAG_TW, SWITCH_OFF) == SWITCH_ON;
    }

    public void saveTWName(int state) {
        encode(KEY_NAME_TW, state);
    }

    public boolean doesTWName() {
        return decodeInt(KEY_NAME_TW, SWITCH_ON) == SWITCH_ON;
    }

    /**
     * 保存控制头像特殊时期不可修改开关
     *
     * @param state 只能是SWITCH_ON或者SWITCH_OFF两种状态
     */
    public void saveEnableModifyUserHeader(int state) {
        encode(KEY_ENABLE_MODIFY_USER_HEADER, state);
    }

    /**
     * 是否打开 头像修改 开关
     *
     * @return true=不可修改
     */
    public boolean doesDisableModifyUserHeader() {
        return decodeInt(KEY_ENABLE_MODIFY_USER_HEADER, SWITCH_OFF) == SWITCH_ON;
    }

    /**
     * 保存控制id特殊时期不可修改开关
     *
     * @param state 只能是SWITCH_ON或者SWITCH_OFF两种状态
     */
    public void saveEnableModifyUserID(int state) {
        encode(KEY_ENABLE_MODIFY_USER_ID, state);
    }

    /**
     * 是否打开 id修改 开关
     *
     * @return true=不可修改
     */
    public boolean doesDisableModifyUserID() {
        return decodeInt(KEY_ENABLE_MODIFY_USER_ID, SWITCH_OFF) == SWITCH_ON;
    }

    /**
     * 保存控制昵称特殊时期不可修改开关
     *
     * @param state 只能是SWITCH_ON或者SWITCH_OFF两种状态
     */
    public void saveEnableModifyUserNick(int state) {
        encode(KEY_ENABLE_MODIFY_USER_NICK, state);
    }

    /**
     * 是否打开 昵称修改 开关
     *
     * @return true=不可修改
     */
    public boolean doesDisableModifyUserNick() {
        return decodeInt(KEY_ENABLE_MODIFY_USER_NICK, SWITCH_OFF) == SWITCH_ON;
    }

    /**
     * 保存控制自我介绍特殊时期不可修改开关
     *
     * @param state 只能是SWITCH_ON或者SWITCH_OFF两种状态
     */
    public void saveEnableModifyIntroduce(int state) {
        encode(KEY_ENABLE_MODIFY_USER_INTRODUCE, state);
    }

    /**
     * 是否打开 自我介绍修改 开关
     *
     * @return true=不可修改
     */
    public boolean doesDisableModifyIntroduce() {
        return decodeInt(KEY_ENABLE_MODIFY_USER_INTRODUCE, SWITCH_OFF) == SWITCH_ON;
    }
}
