package com.penny.common.storage;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.penny.common.di.module.DatabaseModule;
import com.penny.common.storage.local.KVCacheHelper;
import com.penny.common.storage.local.SecuritySharedPreference;
import com.penny.common.storage.memory.Cache;
import com.penny.common.storage.memory.CacheType;
import com.penny.common.util.Preconditions;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Lazy;
import retrofit2.Retrofit;

/**
 * com.penny.commonbase.storage
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe : 网络，数据库，本地
 * @date : 2020/6/4
 */
@Singleton
public class DataRepositoryImpl implements IDataRepository {


    /**
     * 数据库名
     */
    private static final String DEFAULT_DATABASE_NAME = "xx.db";

    @Inject
    Lazy<Retrofit> mRetrofit;

    @Inject
    Application mApplication;

    @Inject
    DatabaseModule.RoomConfiguration mRoomConfiguration;

    @Inject
    Cache.Factory mCachefactory;


    /**
     * 缓存 Retrofit Service
     */
    private Cache mRetrofitServiceCache;

    /**
     * 缓存 RoomDatabase
     */
    private Cache mRoomDatabaseCache;


    /**
     * 缓存 sp
     */
    private Cache mSecuritySp;


    @Inject
    public DataRepositoryImpl() {
    }

    @Override
    public Context getContext() {
        return mApplication;
    }

    /**
     * 获取retrofit 优先取缓存
     *
     * @param service ApiService
     * @param <T>
     * @return
     */
    @Override
    public <T> T getRetrofitService(@NonNull Class<T> service) {
        if (mRetrofitServiceCache == null)
            mRetrofitServiceCache = mCachefactory.build(CacheType.RETROFIT_SERVICE_CACHE);

        Preconditions.checkNotNull(mRetrofitServiceCache, "Cannot return null from a Cache.Factory#build(int) method");
        T retrofitService = (T) mRetrofitServiceCache.get(service.getCanonicalName());
        if (retrofitService == null) {
            synchronized (mRetrofitServiceCache) {
                if (retrofitService == null) {
                    retrofitService = mRetrofit.get().create(service);
                    //缓存
                    mRetrofitServiceCache.put(service.getCanonicalName(), retrofitService);
                }
            }
        }
        return retrofitService;
    }

    /**
     * 获取room 数据库操作对象 优先去缓存
     *
     * @param database
     * @param dbName
     * @param <T>
     * @return
     */
    @Override
    public <T extends RoomDatabase> T getRoomDatabase(@NonNull Class<T> database, @Nullable String dbName) {
        if (mRoomDatabaseCache == null)
            mRoomDatabaseCache = mCachefactory.build(CacheType.ROOM_SERVICE_CACHE);

        Preconditions.checkNotNull(mRoomDatabaseCache, "Cannot return null from a Cache.Factory#build(int) method");
        T roomDatabase = (T) mRoomDatabaseCache.get(database.getCanonicalName());
        if (roomDatabase == null) {
            synchronized (mRoomDatabaseCache) {
                RoomDatabase.Builder<T> builder = Room.databaseBuilder(getContext().getApplicationContext(), database, TextUtils.isEmpty(dbName) ? DEFAULT_DATABASE_NAME : dbName);
                if (mRoomConfiguration != null) {
                    mRoomConfiguration.configRoom(mApplication, builder);
                }
                roomDatabase = builder.build();
                //缓存
                mRoomDatabaseCache.put(database.getCanonicalName(), roomDatabase);
            }
        }
        return roomDatabase;
    }


    /**
     * 获取kvHelper 实体
     *
     * @return
     */
    @Override
    public KVCacheHelper getLocalKVHelper() {
        return KVCacheHelper.getInstance();
    }

    /**
     * 获取加密sp
     *
     * @param name
     * @param mode
     * @return
     */
    @Override
    public SecuritySharedPreference getSecuritySharedPreference(String name, int mode) {
        //todo 后期需要加上缓存
        return new SecuritySharedPreference(mApplication, name, mode);
    }


}
