package com.penny.common.storage;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.RoomDatabase;

import com.penny.common.storage.local.KVCacheHelper;
import com.penny.common.storage.local.SecuritySharedPreference;

/**
 * com.penny.commonbase.storage
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :统一管理数据业务层
 * @date : 2020/6/4
 */
public interface IDataRepository {

    /**
     * 上下文提供
     *
     * @return 上下文对象
     */
    Context getContext();


    /**
     * 获取ApiService
     *
     * @param service ApiService
     * @param <T>
     * @return
     */
    <T> T getRetrofitService(@NonNull Class<T> service);


    /**
     * 数据库操作对象
     *
     * @param database
     * @param dbName
     * @param <T>
     * @return
     */
    <T extends RoomDatabase> T getRoomDatabase(@NonNull Class<T> database, @Nullable String dbName);


    /**
     * 获取本地存储类
     *
     * @return
     */
    KVCacheHelper getLocalKVHelper();


    /**
     * 获取加密的sp
     *
     * @return
     */
    SecuritySharedPreference getSecuritySharedPreference(String name, int mode);

}
