package com.penny.common.storage.local;

import android.content.Context;
import android.text.TextUtils;

import com.orhanobut.logger.Logger;
import com.penny.common.consts.GlobalConst;
import com.tencent.mmkv.MMKV;

import java.util.LinkedHashMap;

import javax.inject.Singleton;

/**
 * Created on 2019/12/17.
 * 用于key-value缓存的帮助类
 *
 * @author wind
 */
public class KVCacheHelper {

    private static final String TAG = "KVCacheHelper";
    private static LinkedHashMap<String, KVCacheModel> cacheModels = new LinkedHashMap<>();


    private KVCacheHelper() {
    }

    private static final class ServiceManagerHolder {
        private static final KVCacheHelper instance = new KVCacheHelper();
    }

    public static KVCacheHelper getInstance() {
        return ServiceManagerHolder.instance;
    }

    public void init(Context context) {
        try {
            MMKV.initialize(context);
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
    }

    public KVCacheModel newModel(String key) {
        if (TextUtils.isEmpty(key)) {
            return null;
        }

        if (cacheModels.containsKey(key)) {
            KVCacheModel model = cacheModels.get(key);
            if (model != null) {
                return model;
            }
        }

        try {
            KVCacheModel model = new KVCacheModel(key);
            cacheModels.put(key, model);
            return model;
        } catch (Exception e) {
            Logger.e(TAG, e);
        }

        return null;
    }

    public KVCacheModel newModelWithCryptKey(String key) {
        if (TextUtils.isEmpty(key)) {
            return null;
        }

        key = key + "_cry";
        if (cacheModels.containsKey(key)) {
            KVCacheModel model = cacheModels.get(key);
            if (model != null) {
                return model;
            }
        }

        try {
            KVCacheModel model = new KVCacheModel(key, GlobalConst.TRANSLATION_KEY);
            cacheModels.put(key, model);
            return model;
        } catch (Exception e) {
            Logger.e(TAG, e);
        }

        return null;
    }

    public KVCacheModel newModelWithMultiCryptKey(String key) {
        if (TextUtils.isEmpty(key)) {
            return null;
        }

        key = key + "_cry";
        if (cacheModels.containsKey(key)) {
            KVCacheModel model = cacheModels.get(key);
            if (model != null) {
                return model;
            }
        }

        try {
            KVCacheModel model = new KVCacheModel(key, MMKV.MULTI_PROCESS_MODE, GlobalConst.TRANSLATION_KEY);
            cacheModels.put(key, model);
            return model;
        } catch (Exception e) {
            Logger.e(TAG, e);
        }

        return null;
    }

    /*todo 这里俩个参数后期需要更改下*/
    public KVCacheModel newGlobalModel(int keyId, boolean keyIsTest) {
        String key = "global_" + keyId + "_" + keyIsTest;
        return newModel(key);
    }


    /**
     * 用于缓存功能开关的值，后面都保存到这里吧
     *
     * @return 缓存对象
     */
    public SwitchKVCacheModel cacheSwitchModel(int keyId, boolean keyIsTest) {
        String key = "function_switch_" + keyId + "_" + keyIsTest;
        if (cacheModels.containsKey(key)) {
            SwitchKVCacheModel model = (SwitchKVCacheModel) cacheModels.get(key);
            if (model != null) {
                return model;
            }
        }

        SwitchKVCacheModel model = new SwitchKVCacheModel(key);
        cacheModels.put(key, model);
        return model;
    }

}