package com.penny.common.router.provider;


import com.penny.common.base.ui.BaseFragment;

/**
 * @ProjectName: SmartWindow
 * @Package: com.smartwindow.core.routerconfig.provide
 * @Description:
 * @Author: Penny
 * @CreateDate: 2020 2020/1/13 12:56
 */
public interface IFragmentProvider extends IBaseProvider {

    BaseFragment newInstance(Object... args);

}
