package com.penny.common.router.provider;

/**
 * com.penny.common.router.provider
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public interface IChatProvider extends IFragmentProvider {

    //服务
    String CHAT_SERVICE = "/chat/service";

    //key
    String CHAT_KEY = "chat_key";

    void setChatData(String data);

}
