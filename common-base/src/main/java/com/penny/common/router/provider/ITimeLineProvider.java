package com.penny.common.router.provider;

/**
 * com.penny.common.router.provider
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public interface ITimeLineProvider extends IFragmentProvider {

    //服务
    String TIME_LINE_SERVICE = "/timeline/service";

    //key
    String TIME_LINE_MOMENT_KEY = "time_line_moment_key";

}
