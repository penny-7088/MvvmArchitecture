package com.penny.common.router.provider;

/**
 * com.penny.common.router.provider
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public interface ISearchProvider extends IFragmentProvider {

    //服务
    String SEARCH_SERVICE = "/search/service";

    //key
    String SEARCH_KEY = "search_key";

}
