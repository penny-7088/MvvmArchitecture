package com.penny.common.router.provider;

import com.alibaba.android.arouter.facade.template.IProvider;

/**
 * com.penny.common.router.provider
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public interface IBaseProvider extends IProvider {
}
