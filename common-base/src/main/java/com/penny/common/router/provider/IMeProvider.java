package com.penny.common.router.provider;

/**
 * com.penny.common.router.provider
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public interface IMeProvider extends IFragmentProvider {

    //服务
    String ME_SERVICE = "/me/service";

    //key
    String ME_KEY = "me_key";

}
