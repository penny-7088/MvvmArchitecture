package com.penny.common.router;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.callback.NavigationCallback;
import com.alibaba.android.arouter.launcher.ARouter;

import java.io.Serializable;

/**
 * com.penny.commonbase.router
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe : 路由跳转封装类
 * @date : 2020/6/5
 */
public class Router {

    private Postcard postcard;

    private Router(Postcard postcard) {
        this.postcard = postcard;
    }

    public static Router newInstance(String path) {
        return new Router(ARouter.getInstance().build(path));
    }

    private void checkPostcard() {
        if (postcard == null)
            throw new IllegalArgumentException("Router  postcard is null");
    }


    /**
     * 携带 Bundle 参数
     *
     * @param bundle
     * @return
     */
    public Router withBundle(Bundle bundle) {
        if (bundle == null) return this;
        checkPostcard();
        postcard.with(bundle);
        return this;
    }

    /**
     * 携带布尔值
     *
     * @param key
     * @param bool
     * @return
     */
    public Router withBoolean(String key, Boolean bool) {
        checkPostcard();
        postcard.withBoolean(key, bool);
        return this;
    }


    /**
     * 携带int 值
     *
     * @param key
     * @param value
     * @return
     */
    public Router withInt(String key, int value) {
        checkPostcard();
        postcard.withInt(key, value);
        return this;
    }

    /**
     * 携带 Serializable
     *
     * @param key
     * @param value
     * @return
     */
    public Router withSerializable(String key, Serializable value) {
        if (value == null) return this;
        checkPostcard();
        postcard.withSerializable(key, value);
        return this;
    }


    /**
     * 携带Parcelable
     *
     * @param key
     * @param value
     * @return
     */
    public Router withParcelable(String key, Parcelable value) {
        if (value == null) return this;
        checkPostcard();
        postcard.withParcelable(key, value);
        return this;
    }


    /**
     * addFlag
     *
     * @param flag
     * @return
     */
    public Router addFlag(int flag) {
        checkPostcard();
        postcard.withFlags(flag);
        return this;
    }


    public Object navigation() {
        return navigation(null);
    }

    public Object navigation(Context context) {
        return navigation(context, null);
    }

    /**
     * 跳转带请求码
     *
     * @param activity
     * @param requestCode
     */
    public void navigation(Activity activity, int requestCode) {
        navigationForResult(activity, requestCode, null);
    }

    /**
     * 跳转 带拦截器
     *
     * @param context
     * @param callback
     * @return
     */
    public Object navigation(Context context, NavigationCallback callback) {
        checkPostcard();
        return postcard.navigation(context, callback);
    }


    /**
     * 跳转带返回参数
     *
     * @param activity
     * @param requestCode
     * @param callback
     */
    public void navigationForResult(Activity activity, int requestCode, NavigationCallback callback) {
        postcard.navigation(activity, requestCode, callback);
    }
}
