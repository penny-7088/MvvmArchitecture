package com.penny.common.router;

import android.os.Bundle;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.launcher.ARouter;
import com.penny.common.router.provider.IChatProvider;
import com.penny.common.router.provider.ILearnProvider;
import com.penny.common.router.provider.IMeProvider;
import com.penny.common.router.provider.ISearchProvider;
import com.penny.common.router.provider.ITimeLineProvider;

/**
 * com.penny.commonbase.router
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe : 管理路由类 通过此类 来解耦每个模块 用于来进行跳转或者传输局
 * @date : 2020/6/5
 */
public class RouterManager {

    @Autowired
    ITimeLineProvider timeLineProvider;

    @Autowired
    ILearnProvider learnProvider;

    @Autowired
    IChatProvider chatProvider;

    @Autowired
    ISearchProvider searchProvider;

    @Autowired
    IMeProvider meProvider;



    private RouterManager() {
        ARouter.getInstance().inject(this);
    }

    private static final class ServiceManagerHolder {
        private static final RouterManager instance = new RouterManager();
    }

    public static RouterManager getInstance() {
        return ServiceManagerHolder.instance;
    }


    public ITimeLineProvider getTimeLineProvider() {
        return timeLineProvider != null ? timeLineProvider : (timeLineProvider = ((ITimeLineProvider) Router.newInstance(timeLineProvider.TIME_LINE_SERVICE).navigation()));
    }

    public ILearnProvider getLearnProvider() {
        return learnProvider != null ? learnProvider : (learnProvider = ((ILearnProvider) Router.newInstance(learnProvider.LEARN_SERVICE).navigation()));
    }

    public IChatProvider getChatProvider() {
        return chatProvider != null ? chatProvider : (chatProvider = ((IChatProvider) Router.newInstance(chatProvider.CHAT_SERVICE).navigation()));
    }


    public IChatProvider getChatProviderArgs(Bundle args) {
        return chatProvider != null ? chatProvider : (chatProvider = ((IChatProvider) Router.newInstance(chatProvider.CHAT_SERVICE).withBundle(args).navigation()));
    }

    public ISearchProvider getSearchProvider() {
        return searchProvider != null ? searchProvider : (searchProvider = ((ISearchProvider) Router.newInstance(searchProvider.SEARCH_SERVICE).navigation()));
    }

    public IMeProvider getMeProvider() {
        return meProvider != null ? meProvider : (meProvider = ((IMeProvider) Router.newInstance(meProvider.ME_SERVICE).navigation()));
    }


}
