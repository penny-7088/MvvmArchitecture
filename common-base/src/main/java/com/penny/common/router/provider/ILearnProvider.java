package com.penny.common.router.provider;

/**
 * com.penny.common.router.provider
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public interface ILearnProvider extends IFragmentProvider {

    //服务
    String LEARN_SERVICE = "/learn/service";

    //key
    String LEARN_KEY = "learn_key";

}
