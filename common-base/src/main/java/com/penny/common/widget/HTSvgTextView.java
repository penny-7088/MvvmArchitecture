package com.penny.common.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatTextView;

import com.penny.common.R;
import com.penny.common.util.CommonUtils;

/**
 * @Author 黄林
 * @Time 2018/7/23 下午6:10
 * @Description
 */
public class HTSvgTextView extends AppCompatTextView {
    private Drawable mDrawableTop;
    private Drawable mDrawableLeft;
    private Drawable mDrawableRight;
    private Drawable mDrawableBottom;
    private int mScaleWidth; // 图片的宽度
    private int mScaleHeight;// 图片的高度

    public HTSvgTextView(Context context) {
        super(context);
    }

    public HTSvgTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public HTSvgTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.common_HTSvgTextView);
        if (null != typedArray) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mDrawableTop = typedArray.getDrawable(R.styleable.common_HTSvgTextView_common_drawableOnTop);
                mDrawableLeft = typedArray.getDrawable(R.styleable.common_HTSvgTextView_common_drawableOnLeft);
                mDrawableRight = typedArray.getDrawable(R.styleable.common_HTSvgTextView_common_drawableOnRight);
                mDrawableBottom = typedArray.getDrawable(R.styleable.common_HTSvgTextView_common_drawableOnBottom);
            } else {
                int mDrawableId;
                mDrawableId = typedArray.getResourceId(R.styleable.common_HTSvgTextView_common_drawableOnTop, -1);
                if (mDrawableId != -1)
                    mDrawableTop = AppCompatResources.getDrawable(context, mDrawableId);
                mDrawableId = typedArray.getResourceId(R.styleable.common_HTSvgTextView_common_drawableOnLeft, -1);
                if (mDrawableId != -1)
                    mDrawableLeft = AppCompatResources.getDrawable(context, mDrawableId);
                mDrawableId = typedArray.getResourceId(R.styleable.common_HTSvgTextView_common_drawableOnRight, -1);
                if (mDrawableId != -1)
                    mDrawableRight = AppCompatResources.getDrawable(context, mDrawableId);
                mDrawableId = typedArray.getResourceId(R.styleable.common_HTSvgTextView_common_drawableOnBottom, -1);
                if (mDrawableId != -1)
                    mDrawableBottom = AppCompatResources.getDrawable(context, mDrawableId);

            }
            mScaleWidth = typedArray
                    .getDimensionPixelOffset(
                            R.styleable.common_HTSvgTextView_common_drawableWidth, 0);
            mScaleHeight = typedArray.getDimensionPixelOffset(
                    R.styleable.common_HTSvgTextView_common_drawableHeight,
                    0);
            typedArray.recycle();
            initDrawable(mDrawableTop);
            initDrawable(mDrawableLeft);
            initDrawable(mDrawableRight);
            initDrawable(mDrawableBottom);
            if (mDrawableTop != null || mDrawableLeft != null || mDrawableRight != null || mDrawableBottom != null) {
                setCompoundOnDrawables(mDrawableLeft, mDrawableTop, mDrawableRight, mDrawableBottom);
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    }

    public void initDrawable(Drawable drawable) {
        if (drawable == null)
            return;
        if (mScaleWidth != 0 && mScaleHeight != 0) {
            drawable.setBounds(0, 0, mScaleWidth, mScaleHeight);
        } else {
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }
    }

    public void setCompoundOnDrawables(@Nullable Drawable left, @Nullable Drawable top,
                                       @Nullable Drawable right, @Nullable Drawable bottom) {
        mDrawableRight = right;
        mDrawableLeft = left;
        mDrawableBottom = bottom;
        mDrawableTop = top;
        if (CommonUtils.isLayoutRtl(getContext())) {
            super.setCompoundDrawables(mDrawableRight, mDrawableTop, mDrawableLeft, mDrawableBottom);
        } else {
            super.setCompoundDrawables(mDrawableLeft, mDrawableTop, mDrawableRight, mDrawableBottom);
        }
    }
}

