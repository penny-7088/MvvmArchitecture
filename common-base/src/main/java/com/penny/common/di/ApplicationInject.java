package com.penny.common.di;

import com.penny.common.app.BaseApplication;

/**
 * com.penny.common.di
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
public interface ApplicationInject {

    void inject(BaseApplication app);
}
