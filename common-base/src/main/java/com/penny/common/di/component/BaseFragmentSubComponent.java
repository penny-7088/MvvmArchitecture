package com.penny.common.di.component;

import androidx.databinding.ViewDataBinding;

import com.penny.common.base.ui.BaseFragment;
import com.penny.common.base.viewmodel.BaseViewModel;

import dagger.Subcomponent;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

/**
 * com.penny.commonbase.di.component
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/4
 */
@Subcomponent(modules = {AndroidInjectionModule.class})
public interface BaseFragmentSubComponent extends AndroidInjector<BaseFragment<BaseViewModel, ViewDataBinding>> {

    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<BaseFragment<BaseViewModel, ViewDataBinding>> {

    }

}
