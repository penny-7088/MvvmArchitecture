package com.penny.common.di.component;

import com.penny.common.di.ApplicationInject;
import com.penny.common.di.module.ViewModelFactoryModule;
import com.penny.common.di.scope.ApplicationScope;
import com.penny.common.di.scope.BaseAppScope;

import dagger.Component;

/**
 * com.penny.common.di.component
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@BaseAppScope
@Component(dependencies = AppComponent.class,modules = {ViewModelFactoryModule.class})
public interface BaseApplicationComponent extends ApplicationInject {

}
