package com.penny.common.di.module;

import android.content.Context;

import androidx.room.RoomDatabase;

import com.penny.common.storage.memory.Cache;
import com.penny.common.storage.memory.CacheType;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * com.penny.commonbase.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/4
 */
@Module
public class DatabaseModule {

    public interface RoomConfiguration<DB extends RoomDatabase> {
        /**
         * 提供接口，自定义配置 RoomDatabase
         *
         * @param context Context
         * @param builder RoomDatabase.Builder
         */
        void configRoom(Context context, RoomDatabase.Builder<DB> builder);

        RoomConfiguration EMPTY = new RoomConfiguration() {
            @Override
            public void configRoom(Context context, RoomDatabase.Builder builder) {

            }
        };
    }

    @Singleton
    @Provides
    static Cache<String, Object> provideExtras(Cache.Factory cacheFactory) {
        return cacheFactory.build(CacheType.EXTRAS);
    }



}
