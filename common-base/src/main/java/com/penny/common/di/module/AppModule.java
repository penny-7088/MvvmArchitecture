package com.penny.common.di.module;

import androidx.lifecycle.ViewModel;

import com.penny.common.base.model.BaseModel;
import com.penny.common.base.viewmodel.BaseViewModel;
import com.penny.common.base.viewmodel.DataViewModel;
import com.penny.common.di.scope.ViewModelScope;
import com.penny.common.router.RouterManager;
import com.penny.common.storage.DataRepositoryImpl;
import com.penny.common.storage.IDataRepository;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import me.jessyan.retrofiturlmanager.RetrofitUrlManager;

/**
 * com.penny.commonbase.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/4
 */
@Module(includes = {GlobalConfigModule.class, HttpModule.class, ViewModelFactoryModule.class, DatabaseModule.class})
public abstract class AppModule {

    @Binds
    abstract IDataRepository bindDataRepository(DataRepositoryImpl dataRepository);

    @Binds
    @IntoMap
    @ViewModelScope(BaseViewModel.class)
    abstract ViewModel bindBaseViewModel(BaseViewModel<? extends BaseModel> viewModel);

    @Binds
    @IntoMap
    @ViewModelScope(DataViewModel.class)
    abstract ViewModel bindDataViewModel(DataViewModel viewModel);


    @Singleton
    @Provides
    static RouterManager provideRouter() {
        return RouterManager.getInstance();
    }


    @Singleton
    @Provides
    static RetrofitUrlManager provideSwitchBaseUrl(){
        return RetrofitUrlManager.getInstance();
    }
}
