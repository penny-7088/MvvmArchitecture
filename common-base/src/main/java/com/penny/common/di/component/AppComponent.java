package com.penny.common.di.component;

import android.app.Application;

import com.penny.common.app.AppDelegateImpl;
import com.penny.common.di.module.AppModule;
import com.penny.common.di.module.GlobalConfigModule;
import com.penny.common.di.module.HttpModule;
import com.penny.common.router.RouterManager;
import com.penny.common.storage.IDataRepository;
import com.penny.common.storage.memory.Cache;

import java.io.File;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * com.penny.commonbase.di
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/1
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    /**
     * 注入委托类
     *
     * @param appDelegate
     */
    void inject(AppDelegateImpl appDelegate);

    /**
     * 全局的app对象
     *
     * @return
     */
    Application getApplication();

    /**
     * 全局获取数据库和网络请求对象
     *
     * @return
     */
    IDataRepository getDataRepository();

    /**
     * ok实例
     * @return
     */
    OkHttpClient okHttpClient();

    /**
     * 缓存
     * @return
     */
    Cache<String, Object> extras();

    /**
     * 路由管理
     */
    RouterManager router();


    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        Builder globalConfigModule(GlobalConfigModule globalConfigModule);

        AppComponent build();
    }
}
