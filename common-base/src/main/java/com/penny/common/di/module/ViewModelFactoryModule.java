package com.penny.common.di.module;

import androidx.lifecycle.ViewModelProvider;

import com.penny.common.base.viewmodel.ViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjectionModule;

/**
 * com.penny.commonbase.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe : ViewModelProvider.Factory 工厂类实列化
 * @date : 2020/6/4
 */
@Module(includes = {AndroidInjectionModule.class})
public abstract class ViewModelFactoryModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory viewModelFactory);
}
