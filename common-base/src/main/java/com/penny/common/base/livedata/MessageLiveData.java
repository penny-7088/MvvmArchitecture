package com.penny.common.base.livedata;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

/**
 * com.penny.commonbase.base.livedata
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/4
 */
public class MessageLiveData extends SingleLiveData<String> {

    public void observe(LifecycleOwner owner, final MessageObserver observer) {
        super.observe(owner, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String t) {
                //过滤空消息
                if (TextUtils.isEmpty(t)) {
                    return;
                }
                observer.onNewMessage(t);
            }
        });
    }

    public interface MessageObserver {
        void onNewMessage(@NonNull String message);
    }
}