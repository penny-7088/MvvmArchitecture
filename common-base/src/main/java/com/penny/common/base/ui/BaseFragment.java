package com.penny.common.base.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import com.orhanobut.logger.Logger;
import com.penny.common.base.livedata.MessageLiveData;
import com.penny.common.base.viewmodel.BaseViewModel;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import dagger.android.support.AndroidSupportInjection;

/**
 * com.penny.commonbase.base.ui
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/4
 */
public abstract class BaseFragment<VM extends BaseViewModel, VDB extends ViewDataBinding> extends Fragment
        implements IView<VM>, HasAndroidInjector {

    @Inject
    DispatchingAndroidInjector<Object> mAndroidInjector;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    protected VM mViewModel;

    protected VDB mBinding;

    protected View mRootView;

    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public boolean isBinding() {
        return true;
    }

    public VM getViewModel() {
        return mViewModel;
    }

    @Override
    public VM createViewModel() {
        return mViewModel;
    }

    public VDB getViewDataBinding() {
        return mBinding;
    }

    protected ViewModelProvider.Factory getViewModelFactory() {
        return mViewModelFactory;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = createRootView(inflater, container, savedInstanceState);
        if (isBinding()) {
            mBinding = DataBindingUtil.bind(mRootView);
            Logger.d("isNull?-->" + mBinding + "---->" + (mBinding == null));
        }
        initViewModel();
        return mRootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData(savedInstanceState);
    }

    protected View createRootView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    private void initViewModel() {
        mViewModel = createViewModel();
        if (mViewModel == null) {
            mViewModel = obtainViewModel(getVMClass());
        }

        if (mViewModel != null) {
            getLifecycle().addObserver(mViewModel);
        }
    }


    @Override
    public AndroidInjector<Object> androidInjector() {
        return mAndroidInjector;
    }

    public <T extends ViewModel> T obtainViewModel(@NonNull Class<T> modelClass) {
        return obtainViewModel(getViewModelStore(), modelClass);
    }

    public <T extends ViewModel> T obtainViewModel(@NonNull ViewModelStore store, @NonNull Class<T> modelClass) {
        return createViewModelProvider(store).get(modelClass);
    }

    private ViewModelProvider createViewModelProvider(@NonNull ViewModelStore store) {
        return new ViewModelProvider(store, mViewModelFactory);
    }

    private Class<VM> getVMClass() {
        Class cls = getClass();
        Class<VM> vmClass = null;
        while (vmClass == null && cls != null) {
            vmClass = getVMClass(cls);
            cls = cls.getSuperclass();
        }
        if (vmClass == null) {
            vmClass = (Class<VM>) BaseViewModel.class;
        }
        return vmClass;
    }

    private Class getVMClass(Class cls) {
        Type type = cls.getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            Type[] types = ((ParameterizedType) type).getActualTypeArguments();
            for (Type t : types) {
                if (t instanceof Class) {
                    Class vmClass = (Class) t;
                    if (BaseViewModel.class.isAssignableFrom(vmClass)) {
                        return vmClass;
                    }
                } else if (t instanceof ParameterizedType) {
                    Type rawType = ((ParameterizedType) t).getRawType();
                    if (rawType instanceof Class) {
                        Class vmClass = (Class) rawType;
                        if (BaseViewModel.class.isAssignableFrom(vmClass)) {
                            return vmClass;
                        }
                    }
                }
            }
        }

        return null;
    }

    /**
     * 注册消息事件
     */
    protected void registerMessageEvent(@NonNull MessageLiveData.MessageObserver observer) {
        mViewModel.getMessageEvent().observe(getViewLifecycleOwner(), observer);
    }

    /**
     * 注册单个消息事件，消息对象:{@link Message}
     *
     * @param observer
     */
    protected void registerSingleLiveEvent(@NonNull Observer<Message> observer) {
        mViewModel.getSingleLiveEvent().observe(getViewLifecycleOwner(), observer);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mViewModel != null) {
            getLifecycle().removeObserver(mViewModel);
            mViewModel = null;
        }

        if (mBinding != null) {
            mBinding.unbind();
        }
    }
}
