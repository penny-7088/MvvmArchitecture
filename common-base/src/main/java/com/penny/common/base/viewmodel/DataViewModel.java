package com.penny.common.base.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.penny.common.base.model.BaseModel;
import com.penny.common.storage.local.KVCacheHelper;
import com.penny.common.storage.memory.Cache;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * com.penny.common.base.viewmodel
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/9
 */
public class DataViewModel extends BaseViewModel<BaseModel> {


    @Inject
    public DataViewModel(@NonNull Application application, BaseModel model) {
        super(application, model);
    }

    /**
     * 传入Class 获得{@link retrofit2.Retrofit#create(Class)} 对应的Class
     *
     * @param service
     * @param <T>
     * @return {@link retrofit2.Retrofit#create(Class)}
     */
    public <T> T getRetrofitService(Class<T> service) {
        return getModel().getRetrofitService(service);
    }

    /**
     * 传入Class 通过{@link Room#databaseBuilder},{@link RoomDatabase.Builder<T>#build()}获得对应的Class
     *
     * @param database
     * @param dbName
     * @param <T>
     * @return {@link RoomDatabase.Builder<T>#build()}
     */
    public <T extends RoomDatabase> T getRoomDatabase(@NonNull Class<T> database, @Nullable String dbName) {
        return getModel().getRoomDatabase(database, dbName);
    }

    /**
     * 获取本地缓存管理类
     *
     * @return {@link KVCacheHelper}
     */
    public KVCacheHelper getKVCacheHelper() {
        return getModel().getKVCache();
    }


    /**
     * 获取 缓存 对象
     */
    public Cache<String, Object> getCache() {
        return getModel().getCache();
    }

}
