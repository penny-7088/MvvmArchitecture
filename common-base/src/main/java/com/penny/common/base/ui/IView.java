package com.penny.common.base.ui;

import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModel;

/**
 * com.penny.commonbase.base.ui
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/4
 */
public interface IView<VM extends ViewModel> {

    /**
     * 根布局id
     *
     * @return
     */
    @LayoutRes
    int getLayoutId();

    /**
     * 初始化数据
     *
     * @param savedInstanceState
     */
    void initData(@Nullable Bundle savedInstanceState);

    /**
     * 是否使用DataBinding
     *
     * @return
     */
    boolean isBinding();

    /**
     * 创建ViewModel
     *
     * @return
     */
    VM createViewModel();
}
