package com.penny.common.base.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;

import com.orhanobut.logger.Logger;
import com.penny.common.base.livedata.MessageLiveData;
import com.penny.common.base.livedata.SingleLiveData;
import com.penny.common.base.livedata.StatusData;
import com.penny.common.base.model.BaseModel;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * com.penny.commonbase.base.viewmodel
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/4
 */
@SuppressLint("TimberArgCount")
public class BaseViewModel<M extends BaseModel> extends AndroidViewModel implements IViewModel {

    private static final String TAG = "BaseViewModel";

    protected M mModel;

    /**
     * 消息事件
     */
    private MessageLiveData mMessageLiveData = new MessageLiveData();
    /**
     * 状态事件
     */
    private StatusData mStatusEvent = new StatusData();

    /**
     * 提供自定义单一消息事件
     */
    private SingleLiveData<Message> mSingleLiveData = new SingleLiveData<>();


    @Inject
    public BaseViewModel(@NonNull Application application, M model) {
        super(application);
        this.mModel = model;
    }

    public M getModel() {
        return this.mModel;
    }


    @Override
    public void onCreate() {
        Logger.d(TAG, "onCreate");


    }

    @Override
    public void onStart() {
        Logger.d(TAG, "onStart");
    }

    @Override
    public void onResume() {
        Logger.d(TAG, "onResume");
    }

    @Override
    public void onPause() {
        Logger.d(TAG, "onPause");
    }

    @Override
    public void onStop() {
        Logger.d(TAG, "onStop");
    }

    @Override
    public void onDestroy() {
        Logger.d(TAG, "onDestroy");
        if (mModel != null) {
            mModel.onDestroy();
            mModel = null;
        }
        mMessageLiveData.call();
        mStatusEvent.call();
        mSingleLiveData.call();
    }

    @Override
    public void onAny(LifecycleOwner owner, Lifecycle.Event event) {
        Timber.d(TAG, "onAny");
    }

    public MessageLiveData getMessageEvent() {
        return mMessageLiveData;
    }

    public SingleLiveData getSingleLiveEvent() {
        return mSingleLiveData;
    }
}
