package com.penny.common.base.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.RoomDatabase;

import com.orhanobut.logger.Logger;
import com.penny.common.storage.IDataRepository;
import com.penny.common.storage.local.KVCacheHelper;
import com.penny.common.storage.memory.Cache;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * com.penny.commonbase.base.model
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe : 处理数据，过滤数据的基类
 * @date : 2020/6/4
 */
public class BaseModel implements IModel {

    private IDataRepository mDataRepository;

    /**
     * 装 disp 的容器 ，防止内存泄漏问题 生命周期的 {@see onDestroy() } 会清空容器
     */
    protected CompositeDisposable mDisposables;


    /**
     * 装载 disp
     *
     * @param disposable
     */
    protected void addSubscription(Disposable disposable) {
        if (disposable == null) return;
        if (mDisposables == null) {
            mDisposables = new CompositeDisposable();
        }
        mDisposables.add(disposable);
    }


    /**
     * 清除disp
     */
    protected void dispose() {
        if (mDisposables != null) {
            mDisposables.clear();
        }
    }


    /**
     * 内存
     */
    @Inject
    Cache<String, Object> mCache;

    @Inject
    public BaseModel(IDataRepository dataRepository) {
        this.mDataRepository = dataRepository;
    }

    @Override
    public void onCreate() {
        Logger.d(mCache);
        Logger.d(getKVCache());

    }

    /**
     * 网络
     *
     * @param service
     * @param <T>
     * @return
     */
    public <T> T getRetrofitService(Class<T> service) {
        return mDataRepository.getRetrofitService(service);
    }

    /**
     * 数据库
     *
     * @param database
     * @param dbName
     * @param <T>
     * @return
     */
    public <T extends RoomDatabase> T getRoomDatabase(@NonNull Class<T> database, @Nullable String dbName) {
        return mDataRepository.getRoomDatabase(database, dbName);
    }

    /**
     * 本地
     *
     * @return
     */
    public KVCacheHelper getKVCache() {
        return mDataRepository.getLocalKVHelper();
    }


    /**
     * 获取缓存
     *
     * @return
     */
    public Cache<String, Object> getCache() {
        return mCache;
    }


    @Override
    public void onDestroy() {
        mDataRepository = null;
        dispose();
    }


}
