package com.penny.common.base.ui;

import android.os.Bundle;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;

import com.penny.common.base.livedata.MessageLiveData;
import com.penny.common.base.viewmodel.BaseViewModel;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

/**
 * com.penny.commonbase.base.ui
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/4
 */
public abstract class BaseActivity<VM extends BaseViewModel, VDB extends ViewDataBinding> extends AppCompatActivity implements IView<VM>, HasAndroidInjector {

    @Inject
    DispatchingAndroidInjector<Object> mAndroidInjector;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    protected VM mViewModel;

    protected VDB mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentPreAction();
        if (isBinding()) {
            mBinding = DataBindingUtil.setContentView(this, getLayoutId());
        } else {
            setContentView(getLayoutId());
        }
        initViewModel();
        initData(savedInstanceState);
    }


    /**
     * 默认绑定 dataBinding
     *
     * @return
     */
    @Override
    public boolean isBinding() {
        return true;
    }

    /**
     * setContent pre Action
     */
    protected void setContentPreAction() {

    }

    private void initViewModel() {
        mViewModel = createViewModel();
        if (mViewModel == null) {
            mViewModel = (VM) obtainViewModel(getVMClass());
        }

        if (mViewModel != null) {
            getLifecycle().addObserver((LifecycleObserver) mViewModel);
        }
    }

    /**
     * 获取viewModel
     *
     * @return 默认返回当前的泛型
     */
    @Override
    public VM createViewModel() {
        return mViewModel;
    }

    /**
     * 获取 ViewDataBinding
     *
     * @return {@link #mBinding}
     */
    public VDB getViewDataBinding() {
        return mBinding;
    }


    public <T extends ViewModel> ViewModel obtainViewModel(@NonNull Class<T> modelClass) {
        return obtainViewModel(getViewModelStore(), modelClass);
    }

    public <T extends ViewModel> T obtainViewModel(@NonNull ViewModelStore store, @NonNull Class<T> modelClass) {
        return createViewModelProvider(store).get(modelClass);
    }

    /**
     * 获取 ViewModelProvider
     *
     * @param store
     * @return
     */
    private ViewModelProvider createViewModelProvider(@NonNull ViewModelStore store) {
        return new ViewModelProvider(store, mViewModelFactory);
    }

    private Class<VM> getVMClass() {
        Class cls = getClass();
        Class<VM> vmClass = null;
        while (vmClass == null && cls != null) {
            vmClass = getVMClass(cls);
            cls = cls.getSuperclass();
        }
        if (vmClass == null) {
            vmClass = (Class<VM>) BaseViewModel.class;
        }
        return vmClass;
    }

    private Class getVMClass(Class cls) {
        Type type = cls.getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            Type[] types = ((ParameterizedType) type).getActualTypeArguments();
            for (Type t : types) {
                if (t instanceof Class) {
                    Class vmClass = (Class) t;
                    if (BaseViewModel.class.isAssignableFrom(vmClass)) {
                        return vmClass;
                    }
                } else if (t instanceof ParameterizedType) {
                    Type rawType = ((ParameterizedType) t).getRawType();
                    if (rawType instanceof Class) {
                        Class vmClass = (Class) rawType;
                        if (BaseViewModel.class.isAssignableFrom(vmClass)) {
                            return vmClass;
                        }
                    }
                }
            }
        }

        return null;
    }


    @Override
    public AndroidInjector<Object> androidInjector() {
        return mAndroidInjector;
    }


    /**
     * 注册消息事件
     */
    protected void registerMessageEvent(@NonNull MessageLiveData.MessageObserver observer) {
        mViewModel.getMessageEvent().observe(this, observer);
    }

    /**
     * 注册单个消息事件，消息对象:{@link Message}
     *
     * @param observer
     */
    protected void registerSingleLiveEvent(@NonNull Observer<Message> observer) {
        mViewModel.getSingleLiveEvent().observe(this, observer);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mViewModel != null) {
            getLifecycle().removeObserver((LifecycleObserver) mViewModel);
            mViewModel = null;
        }

        if (mBinding != null) {
            mBinding.unbind();
        }
    }
}
