package com.penny.common.consts;

/**
 * com.penny.common.consts
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/9
 */
public interface GlobalConst {
    /**
     * 这个加密串不能动，很多地方用到加解密。一动会出大事
     */
    String TRANSLATION_KEY = "15helloTCJTALK20";
}
