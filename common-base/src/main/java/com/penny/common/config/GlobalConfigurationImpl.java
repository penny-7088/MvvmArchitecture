package com.penny.common.config;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;

import com.alibaba.android.arouter.launcher.ARouter;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.penny.common.BuildConfig;
import com.penny.common.app.BaseApplication;
import com.penny.common.app.IAppLifecycle;
import com.penny.common.di.component.DaggerBaseApplicationComponent;
import com.penny.common.di.module.GlobalConfigModule;
import com.penny.common.net.BaseUrl;
import com.penny.common.net.interceptor.http.GlobalHttpHandlerImpl;
import com.penny.common.net.log.RequestInterceptor;
import com.penny.common.storage.local.KVCacheHelper;
import com.penny.common.util.CommonUtils;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import me.jessyan.retrofiturlmanager.RetrofitUrlManager;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Package:     MvvmArchitecture
 * User:        Penny
 * Date:        2020/6/1
 * Description: 在每个组件的Manifest 都需要注册此文件；
 **/
public class GlobalConfigurationImpl implements ConfigModule {


    /**
     * 这里可以根据自己的项目来自定义配置
     *
     * @param context
     * @param builder
     */
    @Override
    public void applyOptions(Context context, GlobalConfigModule.Builder builder) {
        if (!BuildConfig.DEBUG) { //Release 时,让框架不再打印 Http 请求和响应的信息
            builder.printHttpLogLevel(RequestInterceptor.Level.NONE);
        }
        builder.baseurl(BaseUrl.API_URL)
                // 这里提供一个全局处理 Http 请求和响应结果的处理类,可以比客户端提前一步拿到服务器返回的结果,可以做一些操作,比如token超时,重新获取
                .globalHttpHandler(new GlobalHttpHandlerImpl(context))
                .retrofitConfiguration((appContext, retrofitBuilder) -> {//这里可以自己自定义配置Retrofit的参数, 甚至您可以替换框架配置好的 OkHttpClient 对象 (但是不建议这样做, 这样做您将损失框架提供的很多功能)
//                    retrofitBuilder.addConverterFactory(GsonConverterFactory.create());//比如使用fastjson替代gson
                })
                .okhttpConfiguration((appContext, okhttpBuilder) -> {//这里可以自己自定义配置Okhttp的参数
//                    okhttpBuilder.sslSocketFactory(); //支持 Https,详情请百度
                    RetrofitUrlManager.getInstance().with(okhttpBuilder);
                    //cache
                    Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = chain -> {
                        CacheControl.Builder cacheBuilder = new CacheControl.Builder();
                        cacheBuilder.maxAge(0, TimeUnit.SECONDS);
                        cacheBuilder.maxStale(365, TimeUnit.DAYS);
                        CacheControl cacheControl = cacheBuilder.build();
                        Request request = chain.request();
                        if (!CommonUtils.isNetWorkAvailable(appContext)) {
                            request = request.newBuilder()
                                    .cacheControl(cacheControl)
                                    .build();
                        }
                        Response originalResponse = chain.proceed(request);
                        if (CommonUtils.isNetWorkAvailable(appContext)) {
                            int maxAge = 0; // read from cache
                            return originalResponse.newBuilder()
                                    .removeHeader("Pragma")
                                    .header("Cache-Control", "public ,max-age=" + maxAge)
                                    .build();
                        } else {
                            int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
                            return originalResponse.newBuilder()
                                    .removeHeader("Pragma")
                                    .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
                                    .build();
                        }
                    };
                    //cache url
                    File httpCacheDirectory = new File(appContext.getCacheDir(), "responses");
                    int cacheSize = 10 * 1024 * 1024; // 10 MiB
                    Cache cache = new Cache(httpCacheDirectory, cacheSize);
                    okhttpBuilder.connectTimeout(30, TimeUnit.SECONDS);
                    okhttpBuilder.readTimeout(60, TimeUnit.SECONDS);
                    okhttpBuilder.addInterceptor(REWRITE_CACHE_CONTROL_INTERCEPTOR)
                            .cache(cache).build();
                });


    }

    /**
     * 在这里可以做全局都需要用到的第三方库的初始化
     *
     * @param context
     * @param lifecycles
     */
    @Override
    public void injectAppLifecycle(Context context, List<IAppLifecycle> lifecycles) {


    }
}
