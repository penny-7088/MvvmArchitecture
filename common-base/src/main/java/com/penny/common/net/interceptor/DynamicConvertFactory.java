package com.penny.common.net.interceptor;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * com.penny.commonbase.net.interceptor
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/4
 */
public class DynamicConvertFactory extends Converter.Factory {

    private Converter.Factory mFactory;

    public static DynamicConvertFactory create(Converter.Factory factory) {
        if (factory == null) {
            throw new NullPointerException("parameter is null");
        } else {
            return new DynamicConvertFactory(factory);
        }
    }

    private DynamicConvertFactory(Converter.Factory factory) {
        this.mFactory = factory;
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        Class<?> factoryClass = null;
        for (Annotation annotation : annotations) {
            if (annotation instanceof ResponseConverter) {
                factoryClass = ((ResponseConverter) annotation).value();
                break;
            }
        }

        Converter.Factory factory = null;
        if (factoryClass != null) {
            try {
                Method createMethod = factoryClass.getMethod("create");
                factory = (Converter.Factory) createMethod.invoke(null);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        if (factory == null && mFactory != null) {
            factory = mFactory;
        }

        if (factory != null)
            return factory.responseBodyConverter(type, annotations, retrofit);

        return super.responseBodyConverter(type, annotations, retrofit);
    }

    @Override
    public Converter<?, RequestBody> requestBodyConverter(Type type, Annotation[] parameterAnnotations, Annotation[] methodAnnotations, Retrofit retrofit) {
        Class<?> factoryClass = null;
        for (Annotation paramAnno : methodAnnotations) {
            if (paramAnno instanceof RequestConverter) {
                factoryClass = ((RequestConverter) paramAnno).value();
                break;
            }
        }

        Converter.Factory factory = null;
        if (factoryClass != null) {
            try {
                Method createMethod = factoryClass.getMethod("create");
                factory = (Converter.Factory) createMethod.invoke(null);
                return factory.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        if (mFactory != null) {
            factory = mFactory;
        }

        if (factory != null)
            return factory.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit);

        return super.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit);
    }

}