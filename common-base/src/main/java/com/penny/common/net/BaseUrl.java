package com.penny.common.net;

import okhttp3.HttpUrl;

/**
 * com.penny.commonbase.net
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/4
 */
public interface BaseUrl {

    String API_URL = "https://uploadpro.hellotalk8.com";

    /**
     * 在调用 Retrofit API 接口之前,使用 Okhttp 或其他方式,请求到正确的 BaseUrl 并通过此方法返回
     *
     * @return
     */
    HttpUrl url();
}
