package com.penny.common.net.interceptor;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import retrofit2.Converter;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * com.penny.commonbase.net.interceptor
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe : 根据注解类型进行转换
 * @date : 2020/6/4
 */
@Documented
@Target({METHOD})
@Retention(RUNTIME)
public @interface ResponseConverter {
    Class<? extends Converter.Factory> value();
}
