package com.penny.common.net.interceptor.http;

import android.content.Context;

import com.orhanobut.logger.Logger;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * com.penny.commonbase.net.interceptor.http
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe : 全局请求、响应的拦截类
 * @date : 2020/6/4
 */
public class GlobalHttpHandlerImpl implements IGlobalHttpHandler {


    private Context mContext;

    public GlobalHttpHandlerImpl(Context context) {
        this.mContext = context;
    }

    @Override
    public Response onHttpResultResponse(String httpResult, Interceptor.Chain chain, Response response) {
        Logger.d(response);
        return response;
    }


    /**
     * 根据项目的要求可以再这里验证token 做处理
     *
     * @param chain
     * @param request
     * @return
     */
    @Override
    public Request onHttpRequestBefore(Interceptor.Chain chain, Request request) {
        Logger.d(request);
        return request;
    }
}
