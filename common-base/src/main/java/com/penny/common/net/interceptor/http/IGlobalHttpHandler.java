package com.penny.common.net.interceptor.http;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * com.penny.commonbase.net.interceptor.http
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :  全局拦截请求、响应
 * @date : 2020/6/4
 */
public interface IGlobalHttpHandler {

    Response onHttpResultResponse(String httpResult, Interceptor.Chain chain, Response response);

    Request onHttpRequestBefore(Interceptor.Chain chain, Request request);
}
