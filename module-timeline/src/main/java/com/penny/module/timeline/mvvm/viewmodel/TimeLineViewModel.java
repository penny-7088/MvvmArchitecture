package com.penny.module.timeline.mvvm.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.penny.common.base.model.BaseModel;
import com.penny.common.base.viewmodel.DataViewModel;

import javax.inject.Inject;

/**
 * com.penny.module.timeline.mvvm.viewmodel
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public class TimeLineViewModel extends DataViewModel {

    @Inject
    public TimeLineViewModel(@NonNull Application application, BaseModel model) {
        super(application, model);
    }


}
