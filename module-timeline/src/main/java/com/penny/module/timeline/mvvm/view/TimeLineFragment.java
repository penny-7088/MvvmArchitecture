package com.penny.module.timeline.mvvm.view;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.orhanobut.logger.Logger;
import com.penny.common.base.ui.BaseFragment;
import com.penny.module.R;
import com.penny.module.databinding.TimeLineFragmentMomentLayoutBinding;
import com.penny.module.timeline.mvvm.viewmodel.TimeLineViewModel;

/**
 * com.penny.module.timeline.app
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public class TimeLineFragment extends BaseFragment<TimeLineViewModel, TimeLineFragmentMomentLayoutBinding> {

    public static TimeLineFragment newInstance(Object... pArgs) {
        TimeLineFragment lFragment = new TimeLineFragment();
        return lFragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.time_line_fragment_moment_layout;
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        Logger.d("binding:" + mBinding + "<-->mViewModel" + mViewModel);
    }

}
