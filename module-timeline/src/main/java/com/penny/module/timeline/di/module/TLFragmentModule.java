package com.penny.module.timeline.di.module;

import com.penny.common.di.component.BaseFragmentSubComponent;
import com.penny.common.di.scope.FragmentScope;
import com.penny.module.timeline.mvvm.view.TimeLineFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * com.penny.module.timeline.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@Module(subcomponents = BaseFragmentSubComponent.class)
public abstract class TLFragmentModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract TimeLineFragment contributeTimeLineFragment();
}
