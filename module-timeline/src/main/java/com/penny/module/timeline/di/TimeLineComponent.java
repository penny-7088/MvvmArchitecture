package com.penny.module.timeline.di;

import com.penny.common.di.ApplicationInject;
import com.penny.common.di.component.AppComponent;
import com.penny.common.di.component.BaseApplicationComponent;
import com.penny.common.di.scope.ApplicationScope;
import com.penny.module.timeline.di.module.TLModule;

import dagger.Component;

/**
 * com.penny.module.timeline.di
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@ApplicationScope
@Component(dependencies = AppComponent.class, modules = TLModule.class)
public interface TimeLineComponent extends ApplicationInject {


}
