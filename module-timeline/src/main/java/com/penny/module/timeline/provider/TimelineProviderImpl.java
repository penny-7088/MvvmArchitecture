package com.penny.module.timeline.provider;

import android.content.Context;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.penny.common.base.ui.BaseFragment;
import com.penny.common.router.provider.ITimeLineProvider;
import com.penny.module.timeline.mvvm.view.TimeLineFragment;

import static com.penny.common.router.provider.ITimeLineProvider.TIME_LINE_SERVICE;

/**
 * com.penny.module.timeline.provider
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/11
 */
@Route(path = TIME_LINE_SERVICE)
public class TimelineProviderImpl implements ITimeLineProvider {


    @Override
    public TimeLineFragment newInstance(Object... args) {
        return TimeLineFragment.newInstance(args);
    }


    @Override
    public void init(Context context) {

    }
}
