package com.penny.module.timeline.di.module;

import com.penny.common.di.module.ViewModelFactoryModule;

import dagger.Module;

/**
 * com.penny.module.timeline.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@Module(includes = {ViewModelFactoryModule.class, TLFragmentModule.class, TLViewModelModule.class})
public class TLModule {


}
