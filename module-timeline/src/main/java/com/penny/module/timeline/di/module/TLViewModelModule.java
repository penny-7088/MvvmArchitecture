package com.penny.module.timeline.di.module;

import androidx.lifecycle.ViewModel;

import com.penny.common.di.scope.ViewModelScope;
import com.penny.module.timeline.mvvm.viewmodel.TimeLineViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * com.penny.module.timeline.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@Module
public abstract class TLViewModelModule {

    @Binds
    @IntoMap
    @ViewModelScope(TimeLineViewModel.class)
    abstract ViewModel bindContainerViewModel(TimeLineViewModel timeLineViewModel);

}
