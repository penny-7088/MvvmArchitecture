package com.penny.module.timeline.app;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;

import com.penny.common.app.IAppLifecycle;
import com.penny.common.config.ConfigModule;
import com.penny.common.di.module.GlobalConfigModule;

import java.util.List;

/**
 * com.penny.module.timeline.app
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public class TimeLineConfigurationImpl implements ConfigModule {


    @Override
    public void applyOptions(Context context, GlobalConfigModule.Builder builder) {

    }

    @Override
    public void injectAppLifecycle(Context context, List<IAppLifecycle> lifecycles) {
        lifecycles.add(new TimeLineLifecycle());
    }
}
