package com.penny.module.timeline.app;


import androidx.annotation.NonNull;

import com.orhanobut.logger.Logger;
import com.penny.common.app.BaseApplication;
import com.penny.common.app.IAppLifecycle;
import com.penny.module.timeline.di.DaggerTimeLineComponent;

/**
 * com.penny.module.timeline.app
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public class TimeLineLifecycle implements IAppLifecycle {


    @Override
    public void attachBaseContext(@NonNull BaseApplication base) {

    }

    @Override
    public void onCreate(@NonNull BaseApplication application) {
        Logger.d("TimeLineLifecycle 模块 初始化");


    }


    @Override
    public void onTerminate(@NonNull BaseApplication application) {

    }

    @Override
    public void onLowMemory(@NonNull BaseApplication application) {

    }

    @Override
    public void onTrimMemory(@NonNull BaseApplication application, int level) {

    }

    @Override
    public void injectChildModule(@NonNull BaseApplication baseApplication) {
        DaggerTimeLineComponent.builder().appComponent(baseApplication.getAppComponent())
                .build().inject(baseApplication);
    }

}
