package com.penny.module.main.di.module;

import com.penny.common.di.component.BaseFragmentSubComponent;

import dagger.Module;

/**
 * com.penny.mvvm.di.module.fragment
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/12
 */
@Module(subcomponents = BaseFragmentSubComponent.class)
public abstract class ContainerFragmentModule {

}
