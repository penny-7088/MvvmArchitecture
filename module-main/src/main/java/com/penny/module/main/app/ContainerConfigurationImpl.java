package com.penny.module.main.app;

import android.content.Context;

import com.penny.common.app.IAppLifecycle;
import com.penny.common.config.ConfigModule;
import com.penny.common.di.module.GlobalConfigModule;

import java.util.List;

/**
 * com.penny.module.main.app
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/9
 */
public class ContainerConfigurationImpl implements ConfigModule {

    @Override
    public void applyOptions(Context context, GlobalConfigModule.Builder builder) {


    }

    @Override
    public void injectAppLifecycle(Context context, List<IAppLifecycle> lifecycles) {
        lifecycles.add(new ContainerLifecycle());
    }
}
