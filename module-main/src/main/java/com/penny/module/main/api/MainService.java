package com.penny.module.main.api;

import com.google.gson.JsonObject;
import com.penny.common.net.interceptor.RequestConverter;
import com.penny.common.net.interceptor.ResponseConverter;
import com.penny.module.main.config.wns.UserSearchNewPb;

import java.util.List;
import java.util.Map;
import com.penny.module.main.entity.FollowPb;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import okhttp3.RequestBody;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.protobuf.ProtoConverterFactory;
import retrofit2.converter.protobuf.ProtoConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * com.penny.module.main.api
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/16
 */
public interface MainService {

    @ResponseConverter(GsonConverterFactory.class)
    @GET
    Flowable<JsonObject> fetchWNSConfig(@Url String url);


    @POST("/recommender_follow/list")
    Flowable<FollowPb.RelationRecommenderResponse> fetchFriend(@Body RequestBody request);


    @RequestConverter(ProtoConverterFactory.class)
    @GET
    Observable<ResponseBody> fetchSearchUser(@Url String url);
}
