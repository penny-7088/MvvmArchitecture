package com.penny.module.main.mvvm.view;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;

import androidx.annotation.Nullable;

import com.orhanobut.logger.Logger;
import com.penny.common.base.ui.BaseActivity;
import com.penny.common.base.ui.BaseFragment;
import com.penny.common.router.RouterManager;
import com.penny.module.main.R;
import com.penny.module.main.databinding.MainActivityContainerLayoutBinding;
import com.penny.module.main.mvvm.view.adapter.ViewPagerFragmentAdapter;
import com.penny.module.main.mvvm.viewmodel.ContainerViewModel;

import javax.inject.Inject;

/**
 * com.penny.module.main.ui
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/9
 */
public class Container extends BaseActivity<ContainerViewModel, MainActivityContainerLayoutBinding> {

    private SparseArray<BaseFragment> mFragments;

    @Inject
    RouterManager mRouterManager;

    @Override
    public int getLayoutId() {
        return R.layout.main_activity_container_layout;
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {


        initViewPager();
    }

    private void initViewPager() {
        initFragments();
        Logger.d("binding:" + mBinding + "<-->mViewModel" + mViewModel);
        Logger.d(mFragments.size());
        ViewPagerFragmentAdapter adapter = new ViewPagerFragmentAdapter(this, mFragments);
        mBinding.mainVpContainer.setAdapter(adapter);
    }

    private void initBottomTab() {
//        AHBottomNavigationItem navigationItem = new AHBottomNavigationItem("动态", 0);
//        mBinding.mainBnNavigation.addItem(navigationItem);
    }

    private void initFragments() {
        if (mFragments == null)
            mFragments = new SparseArray<BaseFragment>();
//        todo 单独设置数据
//        mRouterManager.getChatProvider().setChatData("你好");
//        todo 跳转时携带数据
//        Bundle bundle = new Bundle();
//        bundle.putString("key","你好");
//        mRouterManager.getChatProviderArgs(bundle).newInstance();
//        todo 不携带任何数据
        mFragments.put(0, mRouterManager.getChatProvider().newInstance("hello"));
        mFragments.put(1, mRouterManager.getTimeLineProvider().newInstance());
//        mFragments.put(1, mRouterManager.getSearchProvider().newInstance());
        mFragments.put(2, mRouterManager.getLearnProvider().newInstance());
//        mFragments.put(2, mRouterManager.getMeProvider().newInstance());
    }


    public void onClick(View v) {
        if (v.getId() == R.id.main_app_login) {
            Logger.d("get wnsConfig");
            mViewModel.obtainWnsConfig(0);
        } else if (v.getId() == R.id.main_app_protobuf) {
            mViewModel.obtainProtobuf();
        }
    }

}
