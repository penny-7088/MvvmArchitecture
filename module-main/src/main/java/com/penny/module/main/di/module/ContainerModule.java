package com.penny.module.main.di.module;

import com.penny.common.di.module.ViewModelFactoryModule;

import dagger.Module;

/**
 * com.penny.module.main.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@Module(includes = {ViewModelFactoryModule.class, ContainerActivityModule.class, ContainerViewModelModule.class})
public class ContainerModule {

}
