package com.penny.module.main.mvvm.mode;

import com.google.gson.JsonObject;
import com.orhanobut.logger.Logger;
import com.penny.common.base.model.BaseModel;
import com.penny.common.storage.IDataRepository;
import com.penny.common.util.RxScheduler;
import com.penny.module.main.api.MainService;
import com.penny.module.main.entity.FollowPb;

import javax.inject.Inject;

import io.reactivex.functions.Consumer;
import me.jessyan.retrofiturlmanager.RetrofitUrlManager;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.penny.common.util.RxScheduler.floIo2Main;

/**
 * com.penny.module.main.mvvm.mode
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/17
 */
public class ContainerModel extends BaseModel {

    public static final String WNS_CONFIG = "/wns_config/v7";
    //数据第三个host，这里需要走get请求，其它走post请求(以后修改host时注意需要确认逻辑是否符合)
    public static final String[] accessServerDomain = {"https://uploadpro.hellotalk8.com" + WNS_CONFIG, "https://52.76.254.249" + WNS_CONFIG, "https://ht-us-cal.s3.amazonaws.com" + WNS_CONFIG};
    //数据第三个host，这里需要走get请求，其它走post请求(以后修改host时注意需要确认逻辑是否符合)
    public static final String[] accessTestServerDomain = {"https://qtest.hellotalk8.com" + WNS_CONFIG, "https://119.28.54.156" + WNS_CONFIG, "https://qtest.hellotalk.org" + WNS_CONFIG};


    @Inject
    public ContainerModel(IDataRepository dataRepository) {
        super(dataRepository);
    }


    public void obtainWnsConfig(final int index) {
//        Logger.d("global" + retrofit);
        MainService retrofitService = getRetrofitService(MainService.class);
        Logger.d(index);
        String param = "/?debug=1&deviceid=69e861634a11f30b1bc0fb879fa159bc";
        accessServerDomain[index] = accessServerDomain[index] + param;
        Logger.d(accessServerDomain[index]);
        addSubscription(retrofitService.fetchWNSConfig(accessServerDomain[index]).compose(floIo2Main())
                .subscribe(result -> obtainWNSConfigSuc(result, index), throwable -> {
                    Logger.d(throwable);
                    iterateWNSConfig(index);
                }));

    }

    private void obtainWNSConfigSuc(JsonObject result, int index) {
        Logger.d("Consumer" + result);
        if (!result.isJsonNull()) {
            Logger.d(result);

            //这个类用于切换baseUrl 前缀地址
            RetrofitUrlManager.getInstance().startAdvancedModel(accessServerDomain[index]);
//            getKVCacheHelper().newModelWithCryptKey()

        } else {
            iterateWNSConfig(index);
        }
    }


    private void iterateWNSConfig(int index) {
        if ((index + 1) <= accessServerDomain.length - 1) {
            obtainWnsConfig(index + 1);
        }
    }


    public void obtainProtobuf() {
//        addSubscription(getRetrofitService(MainService.class)
//                .fetchFriend(creatRequestFbBody())
//                .compose(RxScheduler.floIo2Main())
//                .subscribe(relationRecommenderResponse -> Logger.d(relationRecommenderResponse.toString()),
//                        throwable -> Logger.d(throwable.getMessage())));


    }

    private RequestBody creatRequestFbBody() {
        FollowPb.RelationRecommenderRequest build = FollowPb.RelationRecommenderRequest.newBuilder().setUserid(103488678)
                .setSource("follow_list_recmd")
                .setTab("follow").build();
        RequestBody body = RequestBody.create(MediaType.parse("application/octet-stream"), build.toByteArray());
        return body;
    }
}
