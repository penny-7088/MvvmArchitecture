package com.penny.module.main.app;

import androidx.annotation.NonNull;

import com.penny.common.app.BaseApplication;
import com.penny.common.app.IAppLifecycle;
import com.penny.module.main.di.component.DaggerContainerComponent;

/**
 * com.penny.module.main.app
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe : 主容器的 app 代理类 在这里实现自己需要注入的对象和实体
 * @date : 2020/6/9
 */
public class ContainerLifecycle implements IAppLifecycle {


    @Override
    public void attachBaseContext(@NonNull BaseApplication base) {

    }

    @Override
    public void onCreate(@NonNull BaseApplication application) {


    }

    @Override
    public void onTerminate(@NonNull BaseApplication application) {

    }

    @Override
    public void onLowMemory(@NonNull BaseApplication application) {

    }

    @Override
    public void onTrimMemory(@NonNull BaseApplication application, int level) {

    }

    @Override
    public void injectChildModule(@NonNull BaseApplication baseApplication) {
        DaggerContainerComponent.builder().appComponent(baseApplication.getAppComponent())
                .build().inject(baseApplication);
    }

}
