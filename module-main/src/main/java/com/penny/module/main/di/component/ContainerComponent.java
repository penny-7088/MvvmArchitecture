package com.penny.module.main.di.component;

import com.penny.common.di.ApplicationInject;
import com.penny.common.di.component.AppComponent;
import com.penny.common.di.scope.ContainerScope;
import com.penny.module.main.di.module.ContainerModule;

import dagger.Component;

/**
 * com.penny.module.main.di
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@ContainerScope
@Component(dependencies = AppComponent.class, modules = ContainerModule.class)
public interface ContainerComponent extends ApplicationInject {


}
