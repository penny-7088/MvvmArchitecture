package com.penny.module.main.mvvm.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.penny.common.base.viewmodel.BaseViewModel;
import com.penny.module.main.mvvm.mode.ContainerModel;
import com.google.gson.JsonObject;
import com.orhanobut.logger.Logger;
import com.penny.common.base.model.BaseModel;
import com.penny.common.base.viewmodel.DataViewModel;
import com.penny.common.config.GlobalConfigurationImpl;
import com.penny.common.di.module.GlobalConfigModule;
import com.penny.common.di.module.HttpModule;
import com.penny.common.util.RxScheduler;
import com.penny.module.main.api.MainService;
import com.penny.module.main.config.wns.UserSearchNewPb;
import com.penny.module.main.config.wns.UserSearchPb;

import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.functions.Consumer;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * com.penny.module.main.mvvm.viewmodel
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/9
 */
public class ContainerViewModel extends DataViewModel {

    public static final String WNS_CONFIG = "/wns_config/v7";
    //数据第三个host，这里需要走get请求，其它走post请求(以后修改host时注意需要确认逻辑是否符合)
    public static final String[] accessServerDomain = {"https://uploadpro.hellotalk8.com" + WNS_CONFIG, "https://52.76.254.249" + WNS_CONFIG, "https://ht-us-cal.s3.amazonaws.com" + WNS_CONFIG};
    //数据第三个host，这里需要走get请求，其它走post请求(以后修改host时注意需要确认逻辑是否符合)
    public static final String[] accessTestServerDomain = {"https://qtest.hellotalk8.com" + WNS_CONFIG, "https://119.28.54.156" + WNS_CONFIG, "https://qtest.hellotalk.org" + WNS_CONFIG};

    @Inject
    Retrofit retrofit;

    @Inject
    public ContainerViewModel(@NonNull Application application, BaseModel model) {
        super(application, model);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void obtainWnsConfig(final int index) {
        Logger.d("global" + retrofit);
        MainService retrofitService = getRetrofitService(MainService.class);
        Logger.d(index);
        String param = "/?debug=1&deviceid=69e861634a11f30b1bc0fb879fa159bc";
        accessServerDomain[index] = accessServerDomain[index] + param;
        Logger.d(accessServerDomain[index]);
//        addSubscription(retrofitService.fetchWNSConfig(accessServerDomain[index]).compose(RxScheduler.floIo2Main())
//                .subscribe(result -> obtainWNSConfigSuc(result, index), throwable -> {
//                    Logger.d(throwable);
//                    iterateWNSConfig(index);
//                }));


        retrofitService.fetchSearchUser("https://qtest.hellotalk8.com/go_user_search/v1/recommend/?htntkey=8c5e31d9d18ea70e0405869b5c440437&t=1600349741032&learnlang=6&page=1&sort=default&lang=Chinese&userid=10236219&version=4.1.0&terminaltype=1")
                .compose(RxScheduler.obsIoMain())
                .subscribe(new Consumer<ResponseBody>() {
                    @Override
                    public void accept(ResponseBody searchUserResultRspBodies) throws Exception {
                        Logger.d("succccccccc");
                        InputStream inputStream = searchUserResultRspBodies.byteStream();
                        UserSearchPb.SearchUserResultRspBody.Builder builder = UserSearchPb.SearchUserResultRspBody.newBuilder();

                        UserSearchPb.SearchUserResultRspBody rspBody = UserSearchPb.SearchUserResultRspBody.parseFrom(inputStream);

                        Logger.d(rspBody.toString());

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Logger.d(throwable);
                    }
                });

//        mModel.obtainWnsConfig(index);
    }

    private void obtainWNSConfigSuc(JsonObject result, int index) {
        Logger.d("Consumer" + result);
        if (!result.isJsonNull()) {
            Logger.d(result);
//            getKVCacheHelper().newModelWithCryptKey()

        } else {
            iterateWNSConfig(index);
        }
    }

    private void iterateWNSConfig(int index) {
        if ((index + 1) <= accessServerDomain.length - 1) {
            obtainWnsConfig(index + 1);
        }
    }

}
