package com.penny.module.main.di.module;

import com.penny.common.di.component.BaseActivitySubComponent;
import com.penny.module.main.mvvm.view.Container;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * com.penny.module.main.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@Module(subcomponents = BaseActivitySubComponent.class)
public abstract class ContainerActivityModule {

    @ContributesAndroidInjector
    abstract Container contributeContainerActivity();


}
