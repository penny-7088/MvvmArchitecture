package com.penny.module.main.di.module;

import androidx.lifecycle.ViewModel;

import com.penny.common.di.scope.ViewModelScope;
import com.penny.module.main.mvvm.viewmodel.ContainerViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * com.penny.module.main.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/30
 */
@Module
public abstract class ContainerViewModelModule {

    @Binds
    @IntoMap
    @ViewModelScope(ContainerViewModel.class)
    abstract ViewModel bindContainerViewModel(ContainerViewModel containerViewModel);



}
