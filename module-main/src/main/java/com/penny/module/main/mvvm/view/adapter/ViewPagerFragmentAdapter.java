package com.penny.module.main.mvvm.view.adapter;

import android.util.SparseArray;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.penny.common.base.ui.BaseFragment;

/**
 * com.penny.module.main.mvvm.view.adapter
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/10
 */
public class ViewPagerFragmentAdapter extends FragmentStateAdapter {


    private SparseArray<BaseFragment> mFragments;

    public ViewPagerFragmentAdapter(@NonNull FragmentActivity fragment, SparseArray<BaseFragment> fragments) {
        super(fragment);
        this.mFragments = fragments;
    }


    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getItemCount() {
        return mFragments.size();
    }
}
