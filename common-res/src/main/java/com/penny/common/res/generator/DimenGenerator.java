package com.penny.common.res.generator;

import java.io.File;
import java.io.IOException;

/**
 * com.penny.common.res.generator
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/9
 */
public class DimenGenerator {
    /**
     * 设计稿尺寸(将自己设计师的设计稿的宽度填入)
     */
    private static final int DESIGN_WIDTH = 375;

    /**
     * 设计稿的高度  （将自己设计师的设计稿的高度填入）
     */
    private static final int DESIGN_HEIGHT = 667;

    public static void main(String[] args) throws IOException {
        int smallest = Math.min(DESIGN_WIDTH, DESIGN_HEIGHT);  //求得最小宽度
        DimenTypes[] values = DimenTypes.values();
        for (DimenTypes value : values) {
            File file = new File("");
            MakeUtils.makeAll(smallest, value, file.getCanonicalPath());
        }

    }
}
