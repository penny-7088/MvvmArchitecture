package com.penny.mvvm.di.component;

import com.penny.common.app.BaseApplication;
import com.penny.common.di.component.AppComponent;
import com.penny.common.di.scope.ApplicationScope;
import com.penny.mvvm.app.App;
import com.penny.mvvm.di.module.ApplicationModule;

import dagger.Component;

/**
 * com.penny.mvvm.di.component
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/12
 */
@ApplicationScope //依赖appComponent  modules新创建的module类
@Component(dependencies = AppComponent.class, modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(BaseApplication app);
}
