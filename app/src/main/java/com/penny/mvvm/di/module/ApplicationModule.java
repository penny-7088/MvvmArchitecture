package com.penny.mvvm.di.module;

import com.penny.common.di.module.ViewModelFactoryModule;

import dagger.Module;

/**
 * com.penny.mvvm.di.module
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe : app 的模块类，如果你需要各个模块都需要用到话 可以直接再这里示例话 通过 {@see @Inject xxx 来获取示例}
 * @date : 2020/6/12
 */
@Module(includes = {ViewModelFactoryModule.class, ChildModule.class})
public class ApplicationModule {


}
