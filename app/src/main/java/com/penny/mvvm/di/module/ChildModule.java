package com.penny.mvvm.di.module;

import com.penny.module.chat.di.ChatModule;
import com.penny.module.learn.di.module.LearnModule;
import com.penny.module.main.di.component.ContainerComponent;
import com.penny.module.main.di.module.ContainerModule;
import com.penny.module.timeline.di.module.TLModule;

import dagger.Module;

/**
 * 合并子模块 dagger初始化操作 {@link ContainerComponent 详情看这个例子}
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/7/1
 */
@Module(/*
        *  只有在运行整体app 时 加上 includes = {ContainerModule.class, TLModule.class, LearnModule.class, ChatModule.class} 这块代码块
        *  单独运行每个模块时需要注释此代码
        * */
        includes = {ContainerModule.class, TLModule.class, LearnModule.class, ChatModule.class}
        )
public class ChildModule {



}
