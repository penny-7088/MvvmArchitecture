package com.penny.mvvm.app;

import com.orhanobut.logger.Logger;
import com.penny.common.app.BaseApplication;
import com.penny.mvvm.BuildConfig;
import com.penny.mvvm.di.component.DaggerApplicationComponent;

/**
 * com.penny.mvvm.app
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :  主app
 * @date : 2020/6/12
 */
public class App extends BaseApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        DaggerApplicationComponent.builder().appComponent(getAppComponent())
                .build().inject(this);
        if(BuildConfig.isRunModule){
            mAppDelegateImpl.injectChildModule(this);
        }


    }
}
