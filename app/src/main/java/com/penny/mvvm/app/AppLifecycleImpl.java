package com.penny.mvvm.app;

import android.app.Application;

import androidx.annotation.NonNull;

import com.alibaba.android.arouter.launcher.ARouter;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.penny.common.BuildConfig;
import com.penny.common.app.BaseApplication;
import com.penny.common.app.IAppLifecycle;
import com.penny.common.storage.local.KVCacheHelper;
import com.penny.mvvm.di.component.DaggerApplicationComponent;

import dagger.android.AndroidInjector;
import me.jessyan.retrofiturlmanager.RetrofitUrlManager;
import timber.log.Timber;


/**
 * com.penny.mvvm.app
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe : 每个模块有自己需要初始化的sdk 请自己实现即可 {@see IAppLifecycle}
 * @date : 2020/6/9
 */
public class AppLifecycleImpl implements IAppLifecycle {

    @Override
    public void attachBaseContext(@NonNull BaseApplication base) {

    }

    @Override
    public void onCreate(@NonNull BaseApplication application) {
        Logger.d("init app...");
        KVCacheHelper.getInstance().init(application);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            ARouter.openLog();
            ARouter.openDebug();   // 开启调试模式(如果在InstantRun模式下运行，必须开启调试模式！线上版本需要关闭,否则有安全风险)
            ARouter.printStackTrace();
            RetrofitUrlManager.getInstance().setDebug(true);
            Logger.addLogAdapter(new AndroidLogAdapter());
        }
        ARouter.init(application);
    }

    public void inject(@NonNull BaseApplication application) {
        DaggerApplicationComponent.builder()
                .appComponent(application.getAppComponent())
                .build()
                .inject(application);
    }

    @Override
    public void onTerminate(@NonNull BaseApplication application) {
        ARouter.getInstance().destroy();
    }

    @Override
    public void onLowMemory(@NonNull BaseApplication application) {

    }

    @Override
    public void onTrimMemory(@NonNull BaseApplication application, int level) {

    }

    @Override
    public void injectChildModule(@NonNull BaseApplication baseApplication) {

    }

}
