package com.penny.mvvm.app;

import android.content.Context;

import androidx.room.RoomDatabase;

import com.penny.common.app.IAppLifecycle;
import com.penny.common.config.ConfigModule;
import com.penny.common.di.module.DatabaseModule;
import com.penny.common.di.module.GlobalConfigModule;

import java.util.List;

/**
 * com.penny.mvvm.app
 *
 * @author : Penny (penny@hellotalk.com)
 * @describe :
 * @date : 2020/6/9
 */
public class AppConfigurationImpl implements ConfigModule {

    @Override
    public void applyOptions(Context context, GlobalConfigModule.Builder builder) {
        //todo 这里可以配置自己模块的配置 比如： {@see GlobalConfigModule}
        builder.roomConfiguration(new DatabaseModule.RoomConfiguration() {
            @Override
            public void configRoom(Context context, RoomDatabase.Builder builder) {

            }
        });
    }

    @Override
    public void injectAppLifecycle(Context context, List<IAppLifecycle> lifecycles) {
        lifecycles.add(new AppLifecycleImpl());
    }
}
